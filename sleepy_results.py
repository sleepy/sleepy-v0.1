# sleepy - sleep analysis
# Copyright (C) 2019  MRC Epidemiology Unit, University of Cambridge
#   
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
#   
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#   
# You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
from enum import Enum
from abc import ABC, abstractmethod
import numpy as np
import math
import statistics
from datetime import datetime, time, date, timedelta
import progressbar
#from .sleepy_utilities import *
from copy import deepcopy
import io
import pytz

"""
Results for the whole file
"""
class SleepResult():
    def __init__(self):
        self.days = []

"""
Results for a single day
"""
class SleepDay():
    def __init__(self):
        self.epoch_period = None
        self.timestamps = []
        self.timezone = pytz.UTC
        self.activity_data = []
        self.inclinometer_data = None
        self.sleep_states = []
        self.noon_to_noon = True
        self.timeframe = None
        self.time_in_bed = None
        self.time_out_bed = None
        self.date = None
        self.valid = False
        self.sleeping = None

        self.sleep_parameters = SleepParameters()

    def update_timeframe(self):
        self.timeframe = (datetime.fromtimestamp(self.timestamps[0], tz=self.timezone), datetime.fromtimestamp(self.timestamps[-1], tz=self.timezone))
        midn = int(len(self.timestamps) / 2)
        if self.noon_to_noon:
            # Midnight to midnight
            self.date = datetime.fromtimestamp(self.timestamps[0], tz=self.timezone).date()            
        else:
            # Noon to noon
            self.date = datetime.fromtimestamp(self.timestamps[midn], tz=self.timezone).date()
        
    def update_timestamps(self, epoch_period = 60):
        self.timestamps = np.arange(self.timeframe[0].timestamp(), self.timeframe[1].timestamp(), epoch_period)
        while len(self.timestamps) < len(self.activity_data):
            # add an extra timestamp
            last_timestamp = self.timestamps[-1]
            self.timestamps = np.append(self.timestamps, last_timestamp + epoch_period)

"""
Sleep parameters for a single Day
"""
class SleepParametersOLD():
    def __init__(self):
        
        #Sleep onset as Datetime
        self.sleep_onset = None
        #Sleep offset as Datetime
        self.sleep_offset = None

        #Sleep onset index am
        self.sleep_onset_am_ind = None
        #Sleep offset index am
        self.sleep_offset_am_ind = None        
        #Sleep onset index pm
        self.sleep_onset_pm_ind = None
        #Sleep offset index pm
        self.sleep_offset_pm_ind = None

        # Time in Bed (TIB)
        self.tib = None

        # Onset to Offset interval
        self.oo = None
        
        #Sleep minutes during TIB (SMIN)
        self.smin = None
        #True sleep minutes (TSMIN)
        self.tsmin = None
        #Percent sleep (PSLP)
        self.pslp = None
        #Sleep efficiency (SE)
        self.se = None
        #Sleep onset latency (SOL)
        self.sol = None
        #Latency to persistent sleep (LPS)
        self.lps = None
        #Sleep episodes (SEP)
        self.sep = None
        #Mean sleep episode (MSEP)
        self.msep = None
        #Long sleep episodes (LSEP)
        self.lsep = None
        #Longest sleep episode (LGSEP)
        self.lgsep = None

        # All sleep episodes
        self.sleep_episodes = []

        # All naps (sleep episodes outside of night sleep)
        self.naps = []

        #Wake minutes during TIB (WMIN)
        self.wmin = None
        #Wake after sleep onset (WASO)
        self.waso = None
        #Wake episodes (WEP)
        self.wep = None
        #Mean wake episode (MWEP)
        self.mwep = None
        #Long wake episodes (LWEP)
        self.lwep = None
        #Longest wake episode (LGWEP)
        self.lgwep = None

        # All wake episodes
        self.wake_episodes = []

        # All wakenings (wake episodes inside of night sleep)
        self.wakenings = []

        # All non wear episodes
        self.non_wear = []
        
class SleepParameters():
    def __init__(self):

        self.noon_to_noon = True
        
        # Bedtime as Datetime
        self.bedTime = None

        # Waketime as Datetime
        self.wakeTime = None

        # Time in Bed
        self.timeInBed = None
        
        # Sleep onset as Datetime
        self.sleepOnset = None

        # Sleep offset as Datetime
        self.sleepOffset = None

        # SPT - Sleep Period Time
        self.sleepPeriodTime = None

        # TST - Total Sleep Time
        self.totalSleepTime = None

        # SOL - Sleep Onset Latency
        self.sleepOnsetLatency = None

        # WASO - Wake after sleep onset
        self.wakeAfterSleepOnset = None

        # Sleep efficiency as (TST/TIB)*100
        self.sleepEfficiencyTIB = None

        # Sleep efficiency as (TST/SPT)*100
        self.sleepEfficiencySPT = None

        # Number of night wakenings
        self.nightWakingFrequency = None

        # Sum of minutes scored as night wakenings
        self.nightWakingDuration = None

        # Amount of sleep in a 24-h period
        self.daySleepDuration = None

        self.sleepOnsetInd = None
        self.sleepOffsetInd = None

        self.sleepOnsetAmInd = None
        self.sleepOffsetAmInd = None

        self.sleepOnsetPmInd = None
        self.sleepOffsetPmInd = None

        # All sleep episodes
        self.sleep_episodes = []

        # All naps (sleep episodes outside of night sleep)
        self.naps = []

        # All wake episodes
        self.wake_episodes = []

        # All wakenings (wake episodes inside of night sleep)
        self.wakenings = []

        # All non wear episodes
        self.non_wear = []