# coding=utf-8

'''
Script with article: 
Converting raw accelerometer data to activity counts 
using open source code in Matlab, Python, and R ? a comparison to ActiLife activity counts
Corresponding author Ruben Brondeel (ruben.brondeel@umontreal.ca)

This script calculates the Python version of activity counts, 
The functions are a translation of the Matlab function presented in 
Br?nd JC, Andersen LB, Arvidsson D. Generating ActiGraph Counts from 
Raw Acceleration Recorded by an Alternative Monitor. Med Sci Sports Exerc. 2017.

Python (3.6); run in Eclipse (Oxygen.3a Release (4.7.3a))

'''

# % read in libraries needed for the functions
import math, os
import numpy as np
from scipy import signal
import pandas as pd
import resampy
import progressbar
from copy import deepcopy
from datetime import datetime, time, date, timedelta

from matplotlib import pyplot as plt
from matplotlib import dates as md
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle
from pampro.pampro import Time_Series, Channel
#from sleepy.sleepy_results import SleepParameters
#from sleepy.sleepy_analysis import SleepAnalysis

##predefined filter coefficients, as found by Jan Brond
A_coeff = np.array(
    [1, -4.1637, 7.5712, -7.9805, 5.385, -2.4636, 0.89238, 0.06361, -1.3481, 2.4734, -2.9257, 2.9298, -2.7816, 2.4777,
     -1.6847, 0.46483, 0.46565, -0.67312, 0.4162, -0.13832, 0.019852])
B_coeff = np.array(
    [0.049109, -0.12284, 0.14356, -0.11269, 0.053804, -0.02023, 0.0063778, 0.018513, -0.038154, 0.048727, -0.052577,
     0.047847, -0.046015, 0.036283, -0.012977, -0.0046262, 0.012835, -0.0093762, 0.0034485, -0.00080972, -0.00019623])

def add_count_channels(ts):
    channel_names = ts.get_channel_names()
    if 'X' in channel_names:            
        raw_channel_x = ts.get_channel('X')     
        counts_x = Channel.Channel("counts_x")    
        counts_x_data = raw_to_counts(raw_channel_x)
        timestamp_list = [(raw_channel_x.timeframe[0] + i * timedelta(seconds=1)) for i in range(len(counts_x_data))]
        timestamps = np.array(timestamp_list)
        counts_x.set_contents(counts_x_data, timestamps)
        counts_x.timeframe = raw_channel_x.timeframe
        counts_x.time_period = raw_channel_x.time_period
        counts_x.frequency = 1
        ts.add_channel(counts_x)

    if 'Y' in channel_names:
        raw_channel_y = ts.get_channel('Y')     
        counts_y = Channel.Channel("counts_y")    
        counts_y_data = raw_to_counts(raw_channel_y)
        timestamp_list = [(raw_channel_y.timeframe[0] + i * timedelta(seconds=1)) for i in range(len(counts_y_data))]
        timestamps = np.array(timestamp_list)
        counts_y.set_contents(counts_y_data, timestamps)
        counts_y.timeframe = raw_channel_y.timeframe
        counts_y.time_period = raw_channel_y.time_period
        counts_y.frequency = 1
        ts.add_channel(counts_y)

    if 'Z' in channel_names:
        raw_channel_z = ts.get_channel('Z')     
        counts_z = Channel.Channel("counts_z")    
        counts_z_data = raw_to_counts(raw_channel_z)
        timestamp_list = [(raw_channel_z.timeframe[0] + i * timedelta(seconds=1)) for i in range(len(counts_z_data))]
        timestamps = np.array(timestamp_list)
        counts_z.set_contents(counts_z_data, timestamps)
        counts_z.timeframe = raw_channel_z.timeframe
        counts_z.time_period = raw_channel_z.time_period
        counts_z.frequency = 1
        ts.add_channel(counts_z)

    # Now determine vector magnitude if possible
    channel_names = ts.get_channel_names()
    if 'counts_x' in channel_names and 'counts_y' in channel_names and 'counts_z' in channel_names:
        counts_x_channel = ts.get_channel('counts_x')
        counts_y_channel = ts.get_channel('counts_y')
        counts_z_channel = ts.get_channel('counts_z')
        activity_channel = counts_x_channel.clone_as('vector_magnitude')
        for i in range(len(activity_channel.data)):
            activity_channel.data[i] = math.sqrt(
                math.pow(counts_x_channel.data[i], 2) + math.pow(counts_y_channel.data[i], 2) + math.pow(
                    counts_z_channel.data[i], 2))
        ts.add_channel(activity_channel)

    return ts


def set_config(config, default_config):
    """
    set_config [Combine a number of config parameters]

    Parameters
    ----------
    config : [list]
        [A set of config parameters]
    default_config : [list]
        [A set of config parameters to add if they don't already exist in 'config']

    Returns
    -------
    [list]
        [The combined set of config parameters]
    """
    if config is None:
        config = default_config
    else:
        for key, value in config.items():
            default_config[key] = value
        
        config = default_config

    return config

def pptrunc(data, max_value):
    '''
    Saturate a vector such that no element's absolute value exceeds max_abs_value.
    Current name: absolute_saturate().
      :param data: a vector of any dimension containing numerical data
      :param max_value: a float value of the absolute value to not exceed
      :return: the saturated vector
    '''
    outd = np.where(data > max_value, max_value, data)
    return np.where(outd < -max_value, -max_value, outd)


def trunc(data, min_value):
    '''
    Truncate a vector such that any value lower than min_value is set to 0.
    Current name zero_truncate().
    :param data: a vector of any dimension containing numerical data
    :param min_value: a float value the elements of data should not fall below
    :return: the truncated vector
    '''

    return np.where(data < min_value, 0, data)


def runsum(data, length, threshold, name):
    '''
    Compute the running sum of values in a vector exceeding some threshold within a range of indices.
    Divides the data into len(data)/length chunks and sums the values in excess of the threshold for each chunk.
    Current name run_sum().
    :param data: a 1D numerical vector to calculate the sum of
    :param len: the length of each chunk to compute a sum along, as a positive integer
    :param threshold: a numerical value used to find values exceeding some threshold
    :return: a vector of length len(data)/length containing the excess value sum for each chunk of data
    '''

    N = len(data)
    cnt = int(math.ceil(N / length))

    rs = np.zeros(cnt)

    for n in range(cnt):
        for p in range(length * n, length * (n + 1)):
            if p < N and data[p] >= threshold:
                rs[n] = rs[n] + data[p] - threshold

    return rs


def raw_to_counts(channel, B=B_coeff, A=A_coeff):
    name = channel.name
    data = channel.data
    frequency = channel.frequency

    deadband = 0.068
    sf = 30
    peakThreshold = 2.13
    adcResolution = 0.0164
    integN = 10
    gain = 0.965
    print('\t\tResampling and filtering channel ' + channel.name + '...' )

    if frequency > sf:
        data = resampy.resample(np.asarray(data), frequency, sf)

    B2, A2 = signal.butter(4, np.array([0.01, 7]) / (sf / 2), btype='bandpass')
    dataf = signal.filtfilt(B2, A2, data)

    B = B * gain

    # NB: no need for a loop here as we only have one axis in array
    fx8up = signal.lfilter(B, A, dataf)

    fx8 = pptrunc(fx8up[::3], peakThreshold)  # downsampling is replaced by slicing with step parameter

    return runsum(np.floor(trunc(np.abs(fx8), deadband) / adcResolution), integN, 0, name)

def _REM_sleep_surrounded_by_wake_minutes(sleep_states, index, wake_epochs):
    n_before = sum(sleep_states[0:index-1])
    n_after = sum(sleep_states[index+1:len(sleep_states)-1])

    if (n_before >= wake_epochs) and (n_after >= wake_epochs):
        return True
    else:
        return False

"""
    Required:
        now = TIME to test
        start = first TIME
        finish = second TIME
    Optional: reverse (default = False)
        if reverse = False and the start time is after the function will check over midnight
"""
def in_between(now:time, start:time, end:time, reverse=False):
    if start <= end:
        return start <= now <= end
    else:  # over midnight e.g., 23:30-04:15
        if reverse:
            return end <= now <= start
        else:
            return start <= now or now <= end

def resample_inclinometer(inclinometer_data, old_epoch_period, new_epoch_period):
    if old_epoch_period == new_epoch_period:
        return np.array(inclinometer_data)

    elif old_epoch_period > new_epoch_period:
        # upsample data by dividing epochs into equal sized sub-epochs
        dm = divmod(old_epoch_period, new_epoch_period)
        if dm[1] != 0:
            raise RuntimeWarning("The resampling is not precise")

        step = int(dm[0])
        len_old = len(inclinometer_data)
        len_new = int(len_old * step)

        new_inclinometer_data = np.zeros(len_new, np.int)
        i = 0
        for s in range(0, (len_new - step) + 1, step):
            for n in range(0, step):
                new_inclinometer_data[s + n] = inclinometer_data[i]
            i += 1

    else:
        # downsample data by combining epochs into larger epochs
        dm = divmod(new_epoch_period, old_epoch_period)
        if dm[1] != 0:
            raise RuntimeWarning("The resampling is not precise")

        step = int(dm[0])
        len_old = len(inclinometer_data)
        len_new = int(np.ceil(len_old / step))

        new_inclinometer_data = np.zeros(len_new, np.int)
        i = 0
        for s in range(0, (len(inclinometer_data) - step) + 1, step):
            mval = max(inclinometer_data[s:s + step])            
            new_inclinometer_data[i] = mval
            i += 1

    return new_inclinometer_data

def resample_counts(counts_channel, old_epoch_period, new_epoch_period):
    if old_epoch_period == new_epoch_period:
        return counts_channel

    elif old_epoch_period > new_epoch_period:
        # upsample data by dividing epochs into equal sized sub-epochs
        dm = divmod(old_epoch_period, new_epoch_period)
        if dm[1] != 0:
            raise RuntimeWarning("The resampling is not precise")

        step = int(dm[0])
        len_old = len(counts_channel)
        len_new = int(len_old * step)

        new_counts_channel = np.zeros(len_new, np.int)
        i = 0
        for s in range(0, (len_new - step) + 1, step):
            new_value = float(counts_channel[i]) / step
            if new_value < 1:
                for n in range(0, counts_channel[i]):
                    new_counts_channel[s+n] = 1
            else:
                for n in range(0, step):
                    new_counts_channel[s+n] = new_value

            i += 1

    else:
        # downsample data by combining epochs into larger epochs
        dm = divmod(new_epoch_period, old_epoch_period)
        if dm[1] != 0:
            raise RuntimeWarning("The resampling is not precise")

        step = int(dm[0])
        len_old = len(counts_channel)
        len_new = int(np.ceil(len_old / step))

        new_counts_channel = np.zeros(len_new, np.int)
        i = 0
        for s in range(0, len(counts_channel) - step, step):
            new_counts_channel[i] = sum(counts_channel[s:s + step])
            i += 1

    return new_counts_channel

def resample_sleep_states(sleep_states, old_epoch_period, new_epoch_period):

    if old_epoch_period == new_epoch_period:
        return np.array(sleep_states)

    elif old_epoch_period > new_epoch_period:
        # upsample data by dividing epochs into equal sized sub-epochs
        dm = divmod(old_epoch_period, new_epoch_period)
        if dm[1] != 0:
            raise RuntimeWarning("The resampling is not precise")

        step = int(dm[0])
        len_old = len(sleep_states)
        len_new = int(len_old * step)

        new_sleep_states = np.zeros(len_new, np.int)
        i = 0
        for s in range(0, (len_new - step) + 1, step):
            for n in range(0, step):
                new_sleep_states[s + n] = sleep_states[i]
            i += 1

    else:
        # downsample data by combining epochs into larger epochs
        dm = divmod(new_epoch_period, old_epoch_period)
        if dm[1] != 0:
            raise RuntimeWarning("The resampling is not precise")

        step = int(dm[0])
        len_old = len(sleep_states)
        len_new = int(np.ceil(len_old / step))

        new_sleep_states = np.zeros(len_new, np.int)
        i = 0
        for s in range(0, (len(sleep_states) - step) + 1, step):
            if min(sleep_states[s:s + step]) == 1:
                new_sleep_states[i] = 1
            else:
                new_sleep_states[i] = 0
            i += 1

    return new_sleep_states

def plot_day_sleep(day, savename = None, keepOpen = False):
    current_day = datetime.fromtimestamp((day.timeframe[0].timestamp()+day.timeframe[1].timestamp())/2)

    mval = max(day.activity_data)
    dates = [md.date2num(datetime.fromtimestamp(ts,tz=day.timezone)) for ts in day.timestamps]
    locator = md.AutoDateLocator(minticks=6)
    formatter = md.DateFormatter('%H:%M')
    
    fig, axs = plt.subplots(4, 1)
    fig.tight_layout(pad=2.0)
    axs[0].plot(dates, day.activity_data, 'b-')
    if not day.sleeping is None:
        axs[0].plot(dates, day.sleeping*(mval/1.5), 'g-')
    axs[0].plot(dates, day.sleep_states*(mval/2), 'y-')
    axs[0].xaxis.set_major_locator(locator)
    axs[0].xaxis.set_major_formatter(formatter)
    date_title = current_day.strftime("SLEEP (%d %B %Y)")    
    axs[0].set_title(date_title)
    
    axs[1].plot(dates, day.activity_data, 'b-')
    axs[1].xaxis.set_major_locator(locator)
    axs[1].xaxis.set_major_formatter(formatter)
    date_title = current_day.strftime("WAKENINGS (%d %B %Y)")    
    axs[1].set_title(date_title)

    axs[2].plot(dates, day.activity_data, 'b-')
    axs[2].xaxis.set_major_locator(locator)
    axs[2].xaxis.set_major_formatter(formatter)
    date_title = current_day.strftime("NAPS (%d %B %Y)")    
    axs[2].set_title(date_title)

    axs[3].plot(dates, day.activity_data, 'b-')
    axs[3].xaxis.set_major_locator(locator)
    axs[3].xaxis.set_major_formatter(formatter)
    date_title = current_day.strftime("NON WEAR (%d %B %Y)")    
    axs[3].set_title(date_title)
    
    sleep_parameters = day.sleep_parameters
    
    # Add the overall sleep patches
    if day.noon_to_noon:
        night_sleep_patches = []
        if (sleep_parameters.sleepOnset is not None) and (sleep_parameters.sleepOffset is not None):
            x = md.date2num(sleep_parameters.sleepOnset)
            y = 0
            w = md.date2num(sleep_parameters.sleepOffset)-x
            h = mval
            night_sleep_patches.append(Rectangle((x, y), w, h))
    else:
        night_sleep_patches = []
        if sleep_parameters.sleepOffset is not None:
            x = md.date2num(day.timeframe[0])
            y = 0
            w = md.date2num(sleep_parameters.sleepOffset)-x
            h = mval
            night_sleep_patches.append(Rectangle((x, y), w, h))

        if sleep_parameters.sleepOnset is not None:
            x = md.date2num(sleep_parameters.sleepOnset)
            y = 0
            w = md.date2num(day.timeframe[1])-x
            h = mval
            night_sleep_patches.append(Rectangle((x, y), w, h))

    if len(night_sleep_patches) > 0:
        nsp = PatchCollection(night_sleep_patches, facecolor='g', alpha=0.5, edgecolor='None')
        axs[0].add_collection(nsp)
    
    # Create list for all the wake period patches
    if sleep_parameters.wakenings is not None and len(sleep_parameters.wakenings) > 0:
        wakening_patches = []
        for wakening in sleep_parameters.wakenings:
            x = md.date2num(wakening[3])
            y = 0
            w = md.date2num(wakening[4])-md.date2num(wakening[3])
            h = mval
            wakening_patch = Rectangle((x, y), w, h)
            wakening_patches.append(wakening_patch)

        wpp = PatchCollection(wakening_patches, facecolor='m', alpha=0.5, edgecolor='None')
        axs[1].add_collection(wpp)    

    # Create list for all the sleep period patches
    if sleep_parameters.naps is not None and len(sleep_parameters.naps) > 0:
        nap_patches = []
        for nap in sleep_parameters.naps:
            x = md.date2num(nap[3])
            y = 0
            w = md.date2num(nap[4])-md.date2num(nap[3])
            h = mval
            nap_patch = Rectangle((x, y), w, h)
            nap_patches.append(nap_patch)
                

        spp = PatchCollection(nap_patches, facecolor='c', alpha=0.5, edgecolor='None')
        axs[2].add_collection(spp)

    if sleep_parameters.non_wear is not None:
        non_wear_patches = []
        for non_wear in sleep_parameters.non_wear:
            x = md.date2num(non_wear[3])
            y = 0
            w = md.date2num(non_wear[4])-md.date2num(non_wear[3])
            h = mval
            non_wear_patch = Rectangle((x, y), w, h)
            non_wear_patches.append(non_wear_patch)

        nwpp = PatchCollection(non_wear_patches, facecolor='k', alpha=0.5, edgecolor='None')
        axs[3].add_collection(nwpp)
    
    if savename:
        plt.savefig(savename, dpi=150)

    if keepOpen:
        plt.show()

    plt.close()
    

def export_day(f, sp, info):
    if info is not None:
        info_str = ','.join([str(elem) for elem in info])
        f.write(info_str + ",")
    
    sOn = None
    if sp.sleepOnset:
        sOn = sp.sleepOnset.strftime('%H:%M:%S')

    sOff = None
    if sp.sleepOffset:
        sOff = sp.sleepOffset.strftime('%H:%M:%S')

    sleepData = [sOn, sOff, sp.timeInBed, sp.sleepPeriodTime, sp.totalSleepTime, sp.sleepOnsetLatency, sp.wakeAfterSleepOnset, sp.sleepEfficiencyTIB, sp.sleepEfficiencySPT, sp.nightWakingFrequency, sp.nightWakingDuration, sp.daySleepDuration]
    sleepLine = ','.join([str(elem) for elem in sleepData])

    f.write(sleepLine + "\n")

def export_results(filename, sleepy, psgInfo, info = None, folder=''):
    f = open(filename, "a")

    if sleepy is None:
        sOn = datetime.strptime(psgInfo[3], '%H:%M:%S').strftime('%H:%M:%S')
        sOff = datetime.strptime(psgInfo[4], '%H:%M:%S').strftime('%H:%M:%S')
        sleepData = [info[0], info[1], 'PSG', 'PSG', 'PSG', psgInfo[0], sOn, sOff, psgInfo[2], None, psgInfo[7], psgInfo[8], psgInfo[9], None, psgInfo[15], psgInfo[13], psgInfo[11], None]
        sleepLine = ','.join([str(elem) for elem in sleepData])
        f.write(sleepLine + "\n")
    else:
        info.append(sleepy.scoring_method.name)
        info.append(sleepy.rescoring_method.name)
        info.append(sleepy.calculation_method.name)
        info.append(psgInfo[0])
        psgDate = datetime.strptime(psgInfo[0], '%d/%m/%Y').strftime('%d/%m/%Y')
        
        d = 1
        exported = False
        for day in sleepy.results.days:
            dayDate = day.datetime.strftime('%d/%m/%Y')
            if day.valid and (psgDate == dayDate):
                export_day(f, day.sleep_parameters, info)
                plot_day_sleep(day, folder + str(info[1]) + '-' + str(info[2]) + '-' + str(info[3]) + '-' + str(info[4]) + '-D' + str(d) + '.png')
                exported = True
                
            d += 1

        if not exported:
            f.write("\n")

    f.close()

def plot_results(sleepy, keepOpen = False):

    for day in sleepy.results.days:
        plot_day_sleep(day, None, keepOpen=keepOpen)

def findNonWear(day, sleepParameters, config):
    """
    findNonWear - Find all of the periods defined as non wear

    Parameters
    ----------
    day : object
        The day to analyse
    sleepParameters : SleepParameters
        The sleep parameters - using the sleep onset and offset indices
    config : object
        The current config options

    Returns
    -------
    array
        An array of all of the non wear periods
        [
            [startIndex, endIndex, numberOfMinutes, startTime, endTime]
            [startIndex, endIndex, numberOfMinutes, startTime, endTime]
            [startIndex, endIndex, numberOfMinutes, startTime, endTime]
        ]
    """
    non_wear = []

    non_wear_mins = config['non_wear_mins']

    # Need to adjust periods depending on epoch period
    epoch_period = np.round(day.timestamps[1] - day.timestamps[0])
    epochs_per_minute = 60 / epoch_period

    # Determine non-wear in AM sleep period
    sleepOnset = sleepParameters.sleepOnsetAmInd
    sleepOffset = sleepParameters.sleepOffsetAmInd
    zero_counter = 0    
    s = None
    for i in range(sleepOnset, sleepOffset):
        if day.activity_data[i] == 0:
            zero_counter += 1
            if not s:
                s = i
        else:
            if zero_counter >= epochs_per_minute*non_wear_mins:
                non_wear_count = i-s
                non_wear_minutes = non_wear_count / epochs_per_minute
                non_wear.append([s, i-1, non_wear_minutes, datetime.fromtimestamp(day.timestamps[s], tz=day.timezone), datetime.fromtimestamp(day.timestamps[i-1], tz=day.timezone)])
            zero_counter = 0
            s = None

    if s and zero_counter >= epochs_per_minute*non_wear_mins:
        non_wear_count = sleepOffset-s
        non_wear_minutes = non_wear_count / epochs_per_minute
        non_wear.append([s, sleepOffset-1, non_wear_minutes, datetime.fromtimestamp(day.timestamps[s], tz=day.timezone), datetime.fromtimestamp(day.timestamps[sleepOffset-1], tz=day.timezone)])
    
    # Determine non-wear in PM sleep period
    sleepOnset = sleepParameters.sleepOnsetPmInd
    sleepOffset = sleepParameters.sleepOffsetPmInd
    zero_counter = 0    
    s = None
    for i in range(sleepOnset, sleepOffset):
        if day.activity_data[i] == 0:
            zero_counter += 1
            if not s:
                s = i
        else:
            if zero_counter >= epochs_per_minute*non_wear_mins:
                non_wear_count = i-s
                non_wear_minutes = non_wear_count / epochs_per_minute
                non_wear.append([s, i-1, non_wear_minutes, datetime.fromtimestamp(day.timestamps[s], tz=day.timezone), datetime.fromtimestamp(day.timestamps[i-1], tz=day.timezone)])
            zero_counter = 0
            s = None

    if s and zero_counter >= epochs_per_minute*non_wear_mins:
        non_wear_count = sleepOffset-s
        non_wear_minutes = non_wear_count / epochs_per_minute
        non_wear.append([s, sleepOffset-1, non_wear_minutes, datetime.fromtimestamp(day.timestamps[s], tz=day.timezone), datetime.fromtimestamp(day.timestamps[sleepOffset-1], tz=day.timezone)])

    return non_wear

def findSleepEpisodes(day, sleepStartIndices, sleepStopIndices):

    # Need to adjust periods depending on epoch period
    epoch_period = np.round(day.timestamps[1] - day.timestamps[0])
    epochs_per_minute = 60 / epoch_period

    sleepEpisodes = []

    sleep_offset = -1
    for i in range(0, len(sleepStartIndices)):
        sleep_onset = sleepStartIndices[i]
        if (sleep_onset > sleep_offset):
            sleep_offsets = np.nonzero(sleepStopIndices > sleep_onset)[0]
            if len(sleep_offsets) > 0:
                sleep_offset_ind = sleep_offsets[0]
                sleep_offset = sleepStopIndices[sleep_offset_ind]
                
                minutes = ((sleep_offset-sleep_onset)+1) / epochs_per_minute
                sleepEpisodes.append([sleep_onset, sleep_offset, minutes, datetime.fromtimestamp(day.timestamps[sleep_onset], tz=day.timezone), datetime.fromtimestamp(day.timestamps[sleep_offset], tz=day.timezone)])

    return sleepEpisodes

def findWakeEpisodes(day, sleepStartIndices, sleepStopIndices):

    # Need to adjust periods depending on epoch period
    epoch_period = np.round(day.timestamps[1] - day.timestamps[0])
    epochs_per_minute = 60 / epoch_period

    wakeEpisodes = []

    wake_offset = -1
    for i in range(0, len(sleepStopIndices)):
        wake_onset = sleepStopIndices[i]
        if (wake_onset > wake_offset):
            wake_offsets = np.nonzero(sleepStartIndices > wake_onset)[0]
            if len(wake_offsets) > 0:
                wake_offset_ind = wake_offsets[0]
                wake_offset = sleepStartIndices[wake_offset_ind]

                minutes = ((wake_offset-wake_onset)+1) / epochs_per_minute
                wakeEpisodes.append([wake_onset, wake_offset, minutes, datetime.fromtimestamp(day.timestamps[wake_onset], tz=day.timezone), datetime.fromtimestamp(day.timestamps[wake_offset], tz=day.timezone)])

    return wakeEpisodes

def findNaps(day, sleepParameters, config):

    # Need to adjust periods depending on epoch period
    epoch_period = np.round(day.timestamps[1] - day.timestamps[0])
    epochs_per_minute = 60 / epoch_period

    naps = []

    sleepOffsetAm = sleepParameters.sleepOffsetAmInd 
    sleepOnsetAm = sleepParameters.sleepOnsetAmInd
    sleepOffsetPm = sleepParameters.sleepOffsetPmInd
    sleepOnsetPm = sleepParameters.sleepOnsetPmInd

    for sleepEpisode in sleepParameters.sleep_episodes:
        sleep_onset = sleepEpisode[0]
        sleep_offset = sleepEpisode[1]
        if ((sleep_offset <= sleepOnsetAm) or (sleep_onset >= sleepOffsetAm and sleep_offset <= sleepOnsetPm) or (sleep_onset >= sleepOffsetPm)):
            minutes = ((sleep_offset-sleep_onset)+1) / epochs_per_minute
            if minutes >= config['nap-mins']:
                naps.append([sleep_onset, sleep_offset, minutes, datetime.fromtimestamp(day.timestamps[sleep_onset], tz=day.timezone), datetime.fromtimestamp(day.timestamps[sleep_offset], tz=day.timezone)])

    return naps

def findWakenings(day, sleepParameters, config):

    # Need to adjust periods depending on epoch period
    epoch_period = np.round(day.timestamps[1] - day.timestamps[0])
    epochs_per_minute = 60 / epoch_period

    wakenings = []

    if 'wakening-mins' in config:
        wakeningMins = config['wakening-mins']
    else:
        wakeningMins = 1

    sleepOffset = sleepParameters.sleepOffsetInd 
    sleepOnset = sleepParameters.sleepOnsetInd

    sleepOffsetAm = sleepParameters.sleepOffsetAmInd 
    sleepOnsetAm = sleepParameters.sleepOnsetAmInd
    sleepOffsetPm = sleepParameters.sleepOffsetPmInd
    sleepOnsetPm = sleepParameters.sleepOnsetPmInd

    for wakeEpisode in sleepParameters.wake_episodes:
        wake_onset = wakeEpisode[0]
        wake_offset = wakeEpisode[1]
        if sleepParameters.noon_to_noon:
            if ((wake_onset >= sleepOnset and wake_offset <= sleepOffset)):
                minutes = ((wake_offset-wake_onset)+1) / epochs_per_minute
                if minutes >= wakeningMins:
                    wakenings.append([wake_onset, wake_offset, minutes, datetime.fromtimestamp(day.timestamps[wake_onset], tz=day.timezone), datetime.fromtimestamp(day.timestamps[wake_offset], tz=day.timezone)])
        else:
            if ((wake_onset >= sleepOnsetAm and wake_offset <= sleepOffsetAm) or (wake_onset >= sleepOnsetPm and wake_offset <= sleepOffsetPm)):
                minutes = ((wake_offset-wake_onset)+1) / epochs_per_minute
                if minutes >= wakeningMins:
                    wakenings.append([wake_onset, wake_offset, minutes, datetime.fromtimestamp(day.timestamps[wake_onset], tz=day.timezone), datetime.fromtimestamp(day.timestamps[wake_offset], tz=day.timezone)])

    return wakenings

"""
    sleepEpisodes = []
    wakeEpisodes = []

    last_offset = None
    offset = -1
    for i in range(0, len(sleepStartIndices)):
        onset = sleepStartIndices[i]
        if (onset > offset):
            offset_ind = np.nonzero(sleepStopIndices > onset)[0][0]
            offset = sleepStopIndices[offset_ind]
            
            minutes = ((offset-onset)+1) / epochs_per_minute
            sleepEpisodes.append([onset, offset, minutes, datetime.fromtimestamp(day.timestamps[onset], tz=day.timezone), datetime.fromtimestamp(day.timestamps[offset], tz=day.timezone)])
            if last_offset:
                wake_onset = last_offset
                wake_offset = onset
                # Check if this wakening is during an overnight sleep block
                if ((wake_onset >= sleepOnsetAm and wake_onset <= sleepOffsetAm) and (wake_offset >= sleepOnsetAm and wake_offset <= sleepOffsetAm)) or ((wake_onset >= sleepOnsetPm and wake_onset <= sleepOffsetPm) and (wake_offset >= sleepOnsetPm and wake_offset <= sleepOffsetPm)):
                    minutes = ((wake_offset-wake_onset)+1) / epochs_per_minute
                    if minutes >= 5:
                        wakeEpisodes.append([wake_onset, wake_offset, minutes, datetime.fromtimestamp(day.timestamps[wake_onset], tz=day.timezone), datetime.fromtimestamp(day.timestamps[wake_offset], tz=day.timezone)])
                        
            last_offset = offset
"""