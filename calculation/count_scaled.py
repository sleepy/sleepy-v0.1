# sleepy - sleep analysis
# Copyright (C) 2019  MRC Epidemiology Unit, University of Cambridge
#   
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
#   
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#   
# You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
from enum import Enum
from abc import ABC, abstractmethod
import numpy as np
import math
import statistics
from datetime import datetime, time, date, timedelta
import progressbar
from matplotlib import pyplot as plt
from matplotlib import dates as md
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle
from ..sleepy_utilities import *
from ..sleepy_calculation import *
from copy import deepcopy
import io
from ..sleepy_results import SleepParameters, SleepDay, SleepResult

"""
    The algorithm is initiated using a “time flag” of 7:30 pm for sleep onset (night time sleep) and 6:00 am for sleep offset (morning wake). 
    These flags were determined from the average bedtime and wake times that accurately reflected those for the age of our dataset, but can
    be modified for any age group or individual. 
    The algorithm detects wake “events” as the last minute of 15 continuous minutes of sleep followed by 5 min of awake and sleep “events” as 
    the start of 15 continuous minutes of sleep preceded by 5 min of awake. 
    To detect the bedtime sleep “event” the algorithm first moves 3 h forward to detect the first sleep onset event. If sleep is not detected 
    in the 3 h it moves 2 h backwards to identify the last sleep onset event.     
    If a sleep event is not detected within the 3 h after or 2 h before the chosen bedtime, the chosen bedtime (e.g. 7:30 pm) is used.     
    To detect sleep offset the algorithm performs in a similar way, but attempts to detect a wake time rather than a sleep time.
    """
def calculate_day(day, config = None):
    # Defaults
    default_config = {
        'noon_to_noon':False,
        'non_wear_mins': 20,
        'sleep_onset_pre_wake_mins':5,
        'sleep_onset_post_sleep_mins':15,
        'sleep_offset_pre_sleep_mins':15,
        'sleep_offset_post_wake_mins':5,
        'sleep_onset_search_hours_1':3,
        'sleep_onset_search_hours_2':-2,
        'sleep_offset_search_hours_1':3,
        'sleep_offset_search_hours_2':-2,
        'sleep_onset_default': '19:30:00',
        'sleep_offset_default': '06:00:00',
        'long_sleep_episode_mins': 5,
        'long_wake_episode_mins': 5
    }
    config = set_config(config, default_config)       

    sleep_parameters = SleepParameters()

    sleep_onset_search_hours_1 = config['sleep_onset_search_hours_1']
    sleep_onset_search_hours_2 = config['sleep_onset_search_hours_2']
    sleep_offset_search_hours_1 = config['sleep_offset_search_hours_1']
    sleep_offset_search_hours_2 = config['sleep_offset_search_hours_2']
    long_sleep_episode_mins = config['long_sleep_episode_mins']
    long_wake_episode_mins = config['long_wake_episode_mins']
    
    n = len(day.sleep_states)

    # Need to adjust periods depending on epoch period
    epoch_period = np.round(day.timestamps[1] - day.timestamps[0])
    epochs_per_minute = 60 / epoch_period

    if config['noon_to_noon']:
        current_day = datetime.fromtimestamp(day.timestamps[0], tz=day.timezone)
        midnight_start_timestamp = (datetime(current_day.year, current_day.month, current_day.day, 12, 0, 0, 0, tzinfo=day.timezone)).timestamp()
        midnight_end_timestamp = (datetime(current_day.year, current_day.month, current_day.day, 12, 0, 0, 0, tzinfo=day.timezone) + timedelta(days=1)).timestamp()
    else:
        mid_val = int(n/2)
        current_day = datetime.fromtimestamp(day.timestamps[mid_val], tz=day.timezone)
        midnight_start_timestamp = (datetime(current_day.year, current_day.month, current_day.day, 0, 0, 0, 0, tzinfo=day.timezone)).timestamp()
        midnight_end_timestamp = (datetime(current_day.year, current_day.month, current_day.day, 0, 0, 0, 0, tzinfo=day.timezone) + timedelta(days=1)).timestamp()

    midnight_start_ind = np.nonzero(day.timestamps >= midnight_start_timestamp)[0][0]
    midnight_end_ind = np.nonzero(day.timestamps <= midnight_end_timestamp)[0][-1]

    # Find Sleep onset and offsets
    sleepStartIndices, sleepStopIndices = findSleepStartStop(day, config)
    
    sleeping = np.zeros(day.sleep_states.shape, np.uint8)
    if (min(sleepStartIndices) < min(sleepStopIndices)):
        # start awake
        sleep_state = 0

    else:
        # start sleeping
        sleep_state = 1
    
    for i in range(n):
        if i in sleepStopIndices:
            sleep_state = 0
        elif i in sleepStartIndices:
            sleep_state = 1

        sleeping[i] = sleep_state

    day.sleeping = sleeping
    
    # Find overnight sleep
    if (len(sleepStartIndices) > 0) and (len(sleepStopIndices) > 0):
        sleepOnset = findSleepOnsetOffset(day.timestamps, sleepStartIndices, current_day, day.time_in_bed, sleep_onset_search_hours_1, sleep_onset_search_hours_2, day.timezone)
        if config['noon_to_noon']:
            sleepOffset = findSleepOnsetOffset(day.timestamps, sleepStopIndices, current_day+timedelta(days=1), day.time_out_bed, sleep_offset_search_hours_1, sleep_offset_search_hours_2, day.timezone)
        else:
            sleepOffset = findSleepOnsetOffset(day.timestamps, sleepStopIndices, current_day, day.time_out_bed, sleep_offset_search_hours_1, sleep_offset_search_hours_2, day.timezone)
        
        # Find the index of the overnight sleeps
        sleepOffsetInd = np.nonzero(day.timestamps >= sleepOffset.timestamp())[0]
        if len(sleepOffsetInd) > 0:
            sleepOffsetInd = sleepOffsetInd[0]
        else:
            return day

        # Find the index of the overnight sleeps
        sleepOnsetInd = np.nonzero(day.timestamps >= sleepOnset.timestamp())[0]
        if len(sleepOnsetInd) > 0:
            sleepOnsetInd = sleepOnsetInd[0]
        else:
            return day
    
    else:
        return day

    # Add additional sleep offset and onset indices to aid the plotting
    if sleepStartIndices[0] > sleepStopIndices[0]:
        # First patch would not have a start
        sleepStartIndices = np.append(midnight_start_ind, sleepStartIndices)

    if sleepStartIndices[-1] > sleepStopIndices[-1]:
        # Last patch would not have end
        sleepStopIndices = np.append(sleepStopIndices, midnight_end_ind)

    sleepStartIndices = np.sort(sleepStartIndices)
    sleepStopIndices = np.sort(sleepStopIndices)

    if sleepStartIndices[-1] > sleepStopIndices[-1]:
        # Last patch would not have end
        sleepStopIndices = np.append(sleepStopIndices, n-1)

    # Limit the onsets to be midnight or after
    sleepStartIndices[0] = max(sleepStartIndices[0], midnight_start_ind)

    # Limit the offset to be midnight or before
    sleepStopIndices[-1] = min(sleepStopIndices[-1], midnight_end_ind)
    
    if len(sleepStartIndices) != len(sleepStopIndices):
        raise RuntimeError('The onset and offset list count should match')

    # Find the epochs that are reported as being in bed
    if config['noon_to_noon']:
        in_bed_am_ind = 0
        out_bed_am_ind = 0
        in_bed_am_inds = []

        in_bed_pm_ind = np.nonzero(day.timestamps >= day.time_in_bed.timestamp())[0]
        if len(in_bed_pm_ind) > 0:
            in_bed_pm_ind = in_bed_pm_ind[0]
        out_bed_pm_ind = np.nonzero(day.timestamps >= day.time_out_bed.timestamp())[0]
        if len(out_bed_pm_ind) > 0:
            out_bed_pm_ind = out_bed_pm_ind[0]
        in_bed_pm_inds = range(in_bed_pm_ind, out_bed_pm_ind)
        
        sleepOnsetAm = 0
        sleepOffsetAm = 0
        sleepOnsetPm = sleepOnsetInd
        sleepOffsetPm = sleepOffsetInd

    else:
        in_bed_am_ind = midnight_start_ind
        out_bed_am_ind = np.nonzero(day.timestamps >= day.time_out_bed.timestamp())[0]
        if len(out_bed_am_ind) > 0:
            out_bed_am_ind = out_bed_am_ind[0]

        in_bed_am_inds = range(in_bed_am_ind, out_bed_am_ind)

        in_bed_pm_ind = np.nonzero(day.timestamps >= day.time_in_bed.timestamp())[0]
        if len(in_bed_pm_ind) > 0:
            in_bed_pm_ind = in_bed_pm_ind[0]
        
        out_bed_pm_ind = midnight_end_ind
        in_bed_pm_inds = range(in_bed_pm_ind, out_bed_pm_ind)

        sleepOnsetAm = sleepStartIndices[0]
        sleepOffsetAm = sleepOffsetInd
        sleepOnsetPm = sleepOnsetInd
        sleepOffsetPm = sleepStopIndices[-1]
    
    sleep_parameters.sleepOnset = sleepOnset
    sleep_parameters.sleepOffset = sleepOffset
    sleep_parameters.sleepOnsetInd = sleepOnsetInd
    sleep_parameters.sleepOffsetInd = sleepOffsetInd
    sleep_parameters.sleepOnsetAmInd = sleepOnsetAm
    sleep_parameters.sleepOffsetAmInd = sleepOffsetAm
    sleep_parameters.sleepOnsetPmInd = sleepOnsetPm
    sleep_parameters.sleepOffsetPmInd = sleepOffsetPm

    sleep_parameters.sleep_episodes = findSleepEpisodes(day, sleepStartIndices, sleepStopIndices)
    sleep_parameters.wake_episodes = findWakeEpisodes(day, sleepStartIndices, sleepStopIndices)
    
    # NAPS
    sleep_parameters.naps = findNaps(day, sleep_parameters, config)
    
    # WAKENINGS
    sleep_parameters.wakenings = findWakenings(day, sleep_parameters, config)

    # NON WEAR
    sleep_parameters.non_wear = findNonWear(day, sleep_parameters, config)

    """
    TIB - Time in Bed
    """
    time_in_bed_epochs = len(in_bed_am_inds) + len(in_bed_pm_inds)
    sleep_parameters.timeInBed_am = len(in_bed_am_inds)/epochs_per_minute
    sleep_parameters.timeInBed_pm = len(in_bed_pm_inds)/epochs_per_minute
    sleep_parameters.timeInBed = time_in_bed_epochs/epochs_per_minute

    """
    O-O interval = 
    """
    sleep_parameters.sleepPeriodTime = ((sleepOffsetPm-sleepOnsetPm) + (sleepOffsetAm-sleepOnsetAm)) / epochs_per_minute
    sleep_parameters.sleepPeriodTime_am = (sleepOffsetAm-sleepOnsetAm) / epochs_per_minute
    sleep_parameters.sleepPeriodTime_pm = (sleepOffsetPm-sleepOnsetPm) / epochs_per_minute

    """
    True sleep minutes (TSMIN) = Sum of all sleep minutes between sleep_onset and sleep_offset
    """
    if config['noon_to_noon']:
        tsmin_am = 0
        tsmin_pm = 0
        if sleepOffsetPm >= 0:
            if sleepOnsetPm > 0:
                tsmin_pm = np.sum(day.sleep_states[sleepOnsetPm:sleepOffsetPm])/epochs_per_minute
            else:
                tsmin_pm = np.sum(day.sleep_states[sleepOnsetPm:n])/epochs_per_minute
    else:        
        if sleepOffsetAm >= 0:
            if sleepOnsetAm >= 0:
                tsmin_am = np.sum(day.sleep_states[sleepOnsetAm:sleepOffsetAm])/epochs_per_minute
            else:
                tsmin_am = np.sum(day.sleep_states[0:sleepOffsetAm])/epochs_per_minute

        if sleepOffsetPm >= 0:
            if sleepOnsetPm > 0:
                tsmin_pm = np.sum(day.sleep_states[sleepOnsetPm:sleepOffsetPm])/epochs_per_minute
            else:
                tsmin_pm = np.sum(day.sleep_states[sleepOnsetPm:n])/epochs_per_minute
    
    sleep_parameters.totalSleepTime = tsmin_am + tsmin_pm 
    sleep_parameters.totalSleepTime_am = tsmin_am 
    sleep_parameters.totalSleepTime_pm = tsmin_pm 

    """
    Sleep efficiency (SE TIB) = TSMIN divided by the number of minutes between sleep_onset and sleep_offset then multiplied by 100
    """
    sleep_parameters.sleepEfficiencyTIB = (sleep_parameters.totalSleepTime / sleep_parameters.timeInBed)*100
    sleep_parameters.sleepEfficiencySPT = (sleep_parameters.totalSleepTime / sleep_parameters.sleepPeriodTime)*100

    """
    Latency to persistent sleep (LPS) = The number of minutes between time_in_bed and sleep_onset
    """
    if sleepOnsetPm >= 0:
        sleep_parameters.sleepOnsetLatency = (sleepOnsetPm - in_bed_pm_ind)/epochs_per_minute
    elif sleepOnsetAm >= 0:
        sleep_parameters.sleepOnsetLatency = ((n - in_bed_pm_ind) + sleepOffsetAm)/epochs_per_minute

    sleep_parameters.nightWakingFrequency = len(sleep_parameters.wakenings)

    nightWakingDuration = 0

    """
    Wake episode is:
        [start index, end index, duration in minutes, datetime of start, datetime of end]
    """
    for wake_episode in sleep_parameters.wakenings:
        nightWakingDuration += wake_episode[2]

    sleep_parameters.nightWakingDuration = nightWakingDuration
    
    
    """
    Wake after sleep onset (WASO) = Number of wake minutes between sleep_onset and sleep_offset
    """
    sleep_parameters.wakeAfterSleepOnset = sleep_parameters.sleepPeriodTime - sleep_parameters.totalSleepTime
    
    day.sleep_parameters = sleep_parameters
    day.valid = True

    return day

def findSleepStartStop(day, config):
    """findSleepOnsetOffset.

        find all of the sleep onset and offset events
    """

    sleep_state = SleepWakeState.UNKNOWN

    # Need to adjust periods depending on epoch period
    epoch_period = np.round(day.timestamps[1] - day.timestamps[0])
    epochs_per_minute = int(np.ceil(60 / epoch_period))

    n = len(day.sleep_states)
    if config['noon_to_noon']:
        current_day = datetime.fromtimestamp(day.timestamps[0], tz=day.timezone)
        midnight_start_timestamp = (datetime(current_day.year, current_day.month, current_day.day, 12, 0, 0, 0)).timestamp()
        midnight_end_timestamp = (datetime(current_day.year, current_day.month, current_day.day, 12, 0, 0, 0) + timedelta(days=1)).timestamp()
    else:
        mid_val = int(n/2)
        current_day = datetime.fromtimestamp(day.timestamps[mid_val], tz=day.timezone)
        midnight_start_timestamp = (datetime(current_day.year, current_day.month, current_day.day, 0, 0, 0, 0)).timestamp()
        midnight_end_timestamp = (datetime(current_day.year, current_day.month, current_day.day, 0, 0, 0, 0) + timedelta(days=1)).timestamp()
        
    midnight_start_ind = np.nonzero(day.timestamps >= midnight_start_timestamp)[0][0]
    midnight_end_ind = np.nonzero(day.timestamps <= midnight_end_timestamp)[0][-1]

    sleep_onset_post_sleep_epochs = int(config['sleep_onset_post_sleep_mins'] * epochs_per_minute)
    sleep_offset_post_wake_epochs = int(config['sleep_offset_post_wake_mins'] * epochs_per_minute)

    sleep_onset_indices = []
    sleep_offset_indices = []

    s = 0

    while s < n:
        # If we are AWAKE or UNKNOWN
        if sleep_state is not SleepWakeState.ASLEEP:
            if day.sleep_states[s] == 1:
                # Sleep onset is beginning of 15 minutes of consecutive sleep
                window_start = min(s, n)
                window_end = min(s+sleep_onset_post_sleep_epochs, n)
                        
                # Look for consecutive sleep (i.e. all ones)
                if min(day.sleep_states[window_start:window_end]) == 1:
                    sleep_state = SleepWakeState.ASLEEP                
                    sleep_onset_indices.append(s)
                                                
        # If we are ASLEEP or UNKNOWN
        if sleep_state is not SleepWakeState.AWAKE:
            if day.sleep_states[s] == 0:
                # Sleep offset is sleep followed by 5 minutes of consecutive wake
                window_start = min(s, n)
                window_end = min(s+sleep_offset_post_wake_epochs, n)
                
                awake = True
                # Look for consecutive wake (i.e. any wake epoch per minute)
                for i in range(window_start, window_end, epochs_per_minute):
                    if min(day.sleep_states[i:(i+epochs_per_minute)]) == 1:
                        awake = False  
                        break                      
                
                if awake:
                    sleep_state = SleepWakeState.AWAKE
                    sleep_offset_indices.append(s)
        
        s+=1

    sleep_onset_indices = np.array(sleep_onset_indices)
    sleep_offset_indices = np.array(sleep_offset_indices) 

    ok = (sleep_onset_indices > midnight_start_ind) & (sleep_onset_indices < midnight_end_ind)
    sleep_onset_indices = sleep_onset_indices[ok]

    ok = (sleep_offset_indices > midnight_start_ind) & (sleep_offset_indices < midnight_end_ind)
    sleep_offset_indices = sleep_offset_indices[ok]
    
    return sleep_onset_indices, sleep_offset_indices

def findSleepOnsetOffset(sleep_timestamps, sleep_onsetoffset_indices, current_day, time_inout_bed, sleep_onsetoffset_search_hours_1, sleep_onsetoffset_search_hours_2, timezone):

    sleep_onsetoffset_default = time_inout_bed

    # Find the main night sleep onset event    
    found_sleep_onsetoffset = False
    sleep_onsetoffset_timestamps = sleep_timestamps[sleep_onsetoffset_indices]    
    sleep_onsetoffset_search_base = datetime(current_day.year, current_day.month, current_day.day, sleep_onsetoffset_default.hour, sleep_onsetoffset_default.minute, sleep_onsetoffset_default.second, tzinfo=timezone)
    sleep_onsetoffset_search_base_timestamp = sleep_onsetoffset_search_base.timestamp()

    # Start the first search - normally looking ahead 3 hours
    sleep_onsetoffset_search_extents = np.sort([sleep_onsetoffset_search_base_timestamp, (sleep_onsetoffset_search_base + timedelta(hours=sleep_onsetoffset_search_hours_1)).timestamp()])        
    inds = (sleep_onsetoffset_timestamps > sleep_onsetoffset_search_extents[0]) & (sleep_onsetoffset_timestamps < sleep_onsetoffset_search_extents[1])
    possibles = sleep_onsetoffset_timestamps[inds]
    diff = []
    for i in range(0, len(possibles)):
        diff.append(abs(possibles-sleep_onsetoffset_search_base_timestamp))

    if len(diff) > 0:
        min_ind = np.argmin(diff)
        night_sleep_onsetoffset = datetime.fromtimestamp(possibles[min_ind], tz=timezone)
        found_sleep_onsetoffset = True

    if not found_sleep_onsetoffset:
        # Start the second search - normally looking back 2 hours
        sleep_onsetoffset_search_extents = np.sort([sleep_onsetoffset_search_base_timestamp, (sleep_onsetoffset_search_base + timedelta(hours=sleep_onsetoffset_search_hours_2)).timestamp()])        
        inds = (sleep_onsetoffset_timestamps > sleep_onsetoffset_search_extents[0]) & (sleep_onsetoffset_timestamps < sleep_onsetoffset_search_extents[1])
        possibles = sleep_onsetoffset_timestamps[inds]
        diff = []
        for i in range(0, len(possibles)):
            diff.append(abs(possibles-sleep_onsetoffset_search_base_timestamp))

        if len(diff) > 0:
            min_ind = np.argmin(diff)
            night_sleep_onsetoffset = datetime.fromtimestamp(possibles[min_ind], tz=timezone)
            found_sleep_onsetoffset = True
    
    if not found_sleep_onsetoffset:
        night_sleep_onsetoffset = sleep_onsetoffset_search_base

    return night_sleep_onsetoffset


