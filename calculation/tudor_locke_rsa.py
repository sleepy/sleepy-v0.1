# sleepy - sleep analysis
# Copyright (C) 2019  MRC Epidemiology Unit, University of Cambridge
#   
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
#   
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#   
# You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
from enum import Enum
from abc import ABC, abstractmethod
import numpy as np
import math
import statistics
from datetime import datetime, time, date, timedelta
import progressbar
from matplotlib import pyplot as plt
from matplotlib import dates as md
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle
from ..sleepy_utilities import *
from ..sleepy_calculation import *
from copy import deepcopy
import io
import pytz
from ..sleepy_results import SleepParameters, SleepDay, SleepResult

def calculate_day(day, config = None):
    default_config = {
        'noon_to_noon' : True,
        'non_wear_mins': 90,
        'sleep_onset_minutes': 5,
        'sleep_offset_minutes_1': 20, # 21:40 until 05:00
        'sleep_offset_minutes_2': 10, # 05:00 until 11:58
        'min_sleep_minutes': 160,
        'min_wake_minutes': 20,
        'max_sleep_minutes': 1440,
        'min_nonzero_epochs': 0,
        'long_sleep_episode_mins': 5,
        'long_wake_episode_mins': 5
    }

    fixedConfig = {
        'wakening-mins': 1
    }

    config = set_config(config, default_config)
    config = set_config(fixedConfig, config)

    # If our time into bed is before our time out of bed then we are expecting our 24hr period to
    # contain our sleep period (i.e. noon to noon)
    # Otherwise we expect some sleep in the am from the previous night and some sleep in this pm
    noon_to_noon = False
    if day.time_in_bed < day.time_out_bed:
        noon_to_noon = True
    else:
        #TODO Figure out midnight to midnight
        raise RuntimeError('Currently TUDOR_LOCKE analysis is only configured for noon-to-noon analysis')

    non_wear_mins = config['non_wear_mins']
    min_wake_period_mins = config['min_wake_minutes']
    min_sleep_minutes = config['min_sleep_minutes']
    max_sleep_minutes = config['max_sleep_minutes']
    min_nonzero_epochs = config['min_nonzero_epochs']    
    long_sleep_episode_mins = config['long_sleep_episode_mins']
    long_wake_episode_mins = config['long_wake_episode_mins']

    sleep_state = SleepWakeState.UNKNOWN

    n = len(day.sleep_states)

    # Need to adjust periods depending on epoch period
    epoch_period = day.timestamps[1] - day.timestamps[0]
    epochs_per_minute = 60 / epoch_period

    _sleep_onsets, _sleep_offsets = find_sleep_onset_offset(day, config['sleep_onset_minutes'], config['sleep_offset_minutes_1'], config['sleep_offset_minutes_2'])

    """
    Sleep Onsets = The starting point of the first X continuous minutes of sleep
    """
    sleep_onsets = list()
    if noon_to_noon:
        sleep_onsets.extend(_sleep_onsets)
    else:
        #TODO Figure out midnight to midnight
        raise RuntimeError('Currently TUDOR_LOCKE analysis is only configured for noon-to-noon analysis')

    """
    Sleep Offsets:
        the first of X minutes awake
    """
    sleep_offsets = list()

    if noon_to_noon:
        sleep_offsets.extend(_sleep_offsets)        
    else:
        #TODO Figure out midnight to midnight
        raise RuntimeError('Currently TUDOR_LOCKE analysis is only configured for noon-to-noon analysis')
    
    sleep_offsets.sort()
    
    """
    SPT calculations
    """
    sleep_episodes = []
    stop_sleep = 0
    for start_sleep in sleep_onsets:
        if start_sleep >= stop_sleep:
            # For each sleep onset find the end of the SPT
            inds = np.nonzero(np.array(sleep_offsets) > start_sleep)[0]
            if len(inds) > 0:
                stop_sleep = sleep_offsets[inds[0]]-1
                sleep_count = stop_sleep-start_sleep+1
                sleep_minutes = sleep_count/epochs_per_minute
                sleep_episodes.append([start_sleep, stop_sleep, sleep_minutes, datetime.fromtimestamp(day.timestamps[start_sleep], tz=day.timezone), datetime.fromtimestamp(day.timestamps[stop_sleep], tz=day.timezone)])
    
    # Filter out sleep period less than min_sleep_period (160mins) or more than max_sleep_period (1440mins)
    spt_episodes = []
    for sleep_episode in sleep_episodes:
        if sleep_episode[2] >= min_sleep_minutes and sleep_episode[2] <= max_sleep_minutes:
            spt_episodes.append(sleep_episode)

    # Merge sleep episodes that are separated by less than 20 minutes
    merging = True
    while merging:
        merging = False
        for i in range(0, len(spt_episodes)-1):
            sep = spt_episodes[i+1][0] - spt_episodes[i][1]
            if (sep*epochs_per_minute) < 20:
                spt_episodes[i][1] = spt_episodes[i+1][1]
                spt_episodes[i][2] = spt_episodes[i+1][2]+spt_episodes[i][2]
                spt_episodes[i][4] = spt_episodes[i+1][4]
                spt_episodes.pop(i+1)
                merging = True
                break

    
    overall_sleep_parameters = SleepParameters()
    overall_sleep_parameters.noon_to_noon = True
    all_sleep_parameters = []

    for i in range(0, len(spt_episodes)):
        sleep_parameters = SleepParameters()
    
        sleep_parameters.sleepOnset = spt_episodes[i][3]
        sleep_onset_ind = spt_episodes[i][0]
        sleep_parameters.sleepOnsetInd = sleep_onset_ind
        
        sleep_parameters.sleepOffset = spt_episodes[i][4]
        sleep_offset_ind = spt_episodes[i][1]
        sleep_parameters.sleepOffsetInd = sleep_offset_ind

        """
        O-O interval = Number of minutes between sleep onset and sleep offset
        """
        sleep_parameters.timeInBed = (sleep_offset_ind-sleep_onset_ind)+1 / epochs_per_minute
        sleep_parameters.sleepPeriodTime = sleep_parameters.timeInBed

        """
        Sleep minutes during TIB (SMIN) = Sum of all sleep minutes between time_in_bed and time_out_bed
        """
        sleep_parameters.totalSleepTime = np.sum(day.sleep_states[sleep_onset_ind:sleep_offset_ind+1])/epochs_per_minute

        """
        Sleep Onset Latency
        """
        sleep_parameters.sleepOnsetLatency = 0

        """
        Sleep efficiency (SE) = TSMIN divided by the number of minutes between sleep_onset and sleep_offset then multiplied by 100
        """
        sleep_parameters.sleepEfficiencyTIB = (sleep_parameters.totalSleepTime / sleep_parameters.timeInBed)*100

        """
        Sleep efficiency (SE) = TSMIN divided by the number of minutes between sleep_onset and sleep_offset then multiplied by 100
        """
        sleep_parameters.sleepEfficiencySPT = (sleep_parameters.totalSleepTime / sleep_parameters.sleepPeriodTime)*100
        
        """
        Sleep Episodes
        """
        sleep_episodes=[]
        sleep_count = 0

        for i in range(sleep_onset_ind, sleep_offset_ind+1):
            if day.sleep_states[i] == 1:                
                if sleep_count == 0:
                    start_sleep = i
                sleep_count += 1
            else:
                if sleep_count > 0:
                    sleep_minutes = sleep_count/epochs_per_minute
                    sleep_episodes.append([start_sleep, i-1, sleep_minutes, datetime.fromtimestamp(day.timestamps[start_sleep], tz=day.timezone), datetime.fromtimestamp(day.timestamps[i-1], tz=day.timezone)])
                sleep_count = 0
                
            if i == sleep_offset_ind:
                if sleep_count > 0:
                    sleep_minutes = sleep_count/epochs_per_minute
                    sleep_episodes.append([start_sleep, i-1, sleep_minutes, datetime.fromtimestamp(day.timestamps[start_sleep], tz=day.timezone), datetime.fromtimestamp(day.timestamps[i-1], tz=day.timezone)])

        sleep_parameters.sleep_episodes = sleep_episodes
        overall_sleep_parameters.sleep_episodes.extend(sleep_episodes)

        # Wake minutes
        sleep_parameters.wakeAfterSleepOnset = sleep_parameters.sleepPeriodTime-sleep_parameters.totalSleepTime

        # Wake episodes
        wake_episodes=[]
        wake_count = 0
        for i in range(sleep_onset_ind, sleep_offset_ind+1):
            if day.sleep_states[i] == 0:                
                if wake_count == 0:
                    start_wake = i
                wake_count += 1
            else:
                if wake_count > 0:
                    wake_minutes = wake_count/epochs_per_minute
                    wake_episodes.append([start_wake, i-1, wake_minutes, datetime.fromtimestamp(day.timestamps[start_wake], tz=day.timezone), datetime.fromtimestamp(day.timestamps[i-1], tz=day.timezone)])
                wake_count = 0

            if i == sleep_offset_ind:
                if wake_count > 0:
                    wake_minutes = wake_count/epochs_per_minute
                    wake_episodes.append([start_wake, i-1, wake_minutes, datetime.fromtimestamp(day.timestamps[start_wake], tz=day.timezone), datetime.fromtimestamp(day.timestamps[i-1], tz=day.timezone)])

        sleep_parameters.wake_episodes = wake_episodes
        overall_sleep_parameters.wake_episodes.extend(wake_episodes)

        sleep_parameters.nightWakingFrequency = len(wake_episodes)

        nightWakingDuration = 0

        """
        Wake episode is:
            [start index, end index, duration in minutes, datetime of start, datetime of end]
        """
        for wake_episode in wake_episodes:
            nightWakingDuration += wake_episode[2]

        sleep_parameters.nightWakingDuration = nightWakingDuration

        all_sleep_parameters.append(sleep_parameters)

    non_wear = []

    # Detect periods of non-wear
    s_ind = 0
    e_ind = len(day.activity_data)

    zero_counter = 0
    non_zero_counter = 0

    s = None
    for i in range(s_ind, e_ind):
        if day.activity_data[i] == 0:
            zero_counter += 1
            if not s:
                s = i
        else:
            non_zero_counter += 1

            if zero_counter >= epochs_per_minute*non_wear_mins:
                non_wear_count = i-s
                non_wear_minutes = non_wear_count / epochs_per_minute
                non_wear.append([s, i-1, non_wear_minutes, datetime.fromtimestamp(day.timestamps[s], tz=day.timezone), datetime.fromtimestamp(day.timestamps[i-1], tz=day.timezone)])
                zero_counter = 0
                non_zero_counter = 0
                s = None

            if non_zero_counter > epochs_per_minute*2:
                zero_counter = 0
                non_zero_counter = 0
                s = None

    if s and zero_counter >= epochs_per_minute*non_wear_mins:
        non_wear_count = e_ind-s
        non_wear_minutes = non_wear_count / epochs_per_minute
        non_wear.append([s, e_ind-1, non_wear_minutes, datetime.fromtimestamp(day.timestamps[s], tz=day.timezone), datetime.fromtimestamp(day.timestamps[e_ind-1], tz=day.timezone)])

    overall_sleep_parameters.non_wear = non_wear

    # Combine RSA SPT parameters into one for comparison to other analysis methods
    overall_sleep_parameters.sleepOnset = all_sleep_parameters[0].sleepOnset
    sleep_onset_ind = all_sleep_parameters[0].sleepOnsetInd
    overall_sleep_parameters.sleepOnsetInd = sleep_onset_ind

    overall_sleep_parameters.sleepOffset = all_sleep_parameters[-1].sleepOffset
    sleep_offset_ind = all_sleep_parameters[-1].sleepOffsetInd
    overall_sleep_parameters.sleepOffsetInd = sleep_offset_ind

    """
    O-O interval = Number of minutes between sleep onset and sleep offset
    """
    overall_sleep_parameters.timeInBed = (sleep_offset_ind-sleep_onset_ind)+1 / epochs_per_minute
    overall_sleep_parameters.sleepPeriodTime = overall_sleep_parameters.timeInBed

    """
    Sleep minutes during TIB (SMIN) = Sum of all sleep minutes between time_in_bed and time_out_bed
    """
    overall_sleep_parameters.totalSleepTime = np.sum(day.sleep_states[sleep_onset_ind:sleep_offset_ind+1])/epochs_per_minute

    """
    Sleep Onset Latency
    """
    overall_sleep_parameters.sleepOnsetLatency = 0

    """
    Sleep efficiency (SE) = TSMIN divided by the number of minutes between sleep_onset and sleep_offset then multiplied by 100
    """
    overall_sleep_parameters.sleepEfficiencyTIB = (overall_sleep_parameters.totalSleepTime / overall_sleep_parameters.timeInBed)*100

    """
    Sleep efficiency (SE) = TSMIN divided by the number of minutes between sleep_onset and sleep_offset then multiplied by 100
    """
    overall_sleep_parameters.sleepEfficiencySPT = (overall_sleep_parameters.totalSleepTime / overall_sleep_parameters.sleepPeriodTime)*100
        
    """
    Sleep Episodes
    """
    sleep_episodes=[]
    sleep_count = 0

    for i in range(sleep_onset_ind, sleep_offset_ind+1):
        if day.sleep_states[i] == 1:                
            if sleep_count == 0:
                start_sleep = i
            sleep_count += 1
        else:
            if sleep_count > 0:
                sleep_minutes = sleep_count/epochs_per_minute
                sleep_episodes.append([start_sleep, i-1, sleep_minutes, datetime.fromtimestamp(day.timestamps[start_sleep], tz=day.timezone), datetime.fromtimestamp(day.timestamps[i-1], tz=day.timezone)])
            sleep_count = 0
            
        if i == sleep_offset_ind:
            if sleep_count > 0:
                sleep_minutes = sleep_count/epochs_per_minute
                sleep_episodes.append([start_sleep, i-1, sleep_minutes, datetime.fromtimestamp(day.timestamps[start_sleep], tz=day.timezone), datetime.fromtimestamp(day.timestamps[i-1], tz=day.timezone)])

    overall_sleep_parameters.sleep_episodes = sleep_episodes

    # Wake minutes
    overall_sleep_parameters.wakeAfterSleepOnset = overall_sleep_parameters.sleepPeriodTime-overall_sleep_parameters.totalSleepTime

    # Wake episodes
    wake_episodes=[]
    wake_count = 0
    for i in range(sleep_onset_ind, sleep_offset_ind+1):
        if day.sleep_states[i] == 0:                
            if wake_count == 0:
                start_wake = i
            wake_count += 1
        else:
            if wake_count > 0:
                wake_minutes = wake_count/epochs_per_minute
                wake_episodes.append([start_wake, i-1, wake_minutes, datetime.fromtimestamp(day.timestamps[start_wake], tz=day.timezone), datetime.fromtimestamp(day.timestamps[i-1], tz=day.timezone)])
            wake_count = 0

        if i == sleep_offset_ind:
            if wake_count > 0:
                wake_minutes = wake_count/epochs_per_minute
                wake_episodes.append([start_wake, i-1, wake_minutes, datetime.fromtimestamp(day.timestamps[start_wake], tz=day.timezone), datetime.fromtimestamp(day.timestamps[i-1], tz=day.timezone)])

    overall_sleep_parameters.wake_episodes = wake_episodes

    wakenings = findWakenings(day, overall_sleep_parameters, config)

    overall_sleep_parameters.wakenings = wakenings

    overall_sleep_parameters.nightWakingFrequency = len(wakenings)

    nightWakingDuration = 0

    """
    Wake episode is:
        [start index, end index, duration in minutes, datetime of start, datetime of end]
    """
    for wakening in wakenings:
        nightWakingDuration += wakening[2]

    overall_sleep_parameters.nightWakingDuration = nightWakingDuration

    day.sleep_parameters = overall_sleep_parameters
    day.valid = True

    return day


def find_sleep_onset_offset(day, sleep_onset_minutes, sleep_offset_minutes_1, sleep_offset_minutes_2):

    onset_period_start_dt = (datetime(day.timeframe[0].year, day.timeframe[0].month, day.timeframe[0].day, 19, 0, 0, tzinfo=day.timezone))
    onset_period_end_dt = (datetime(day.timeframe[0].year, day.timeframe[0].month, day.timeframe[0].day, 6, 0, 0, tzinfo=day.timezone)+timedelta(days=1))

    onset_period_start = onset_period_start_dt.timestamp()
    onset_period_end = onset_period_end_dt.timestamp()

    offset_period1_start_dt = (datetime(day.timeframe[0].year, day.timeframe[0].month, day.timeframe[0].day, 19, 0, 0, tzinfo=day.timezone))
    offset_period1_end_dt = (datetime(day.timeframe[0].year, day.timeframe[0].month, day.timeframe[0].day, 5, 0, 0, tzinfo=day.timezone)+timedelta(days=1))

    offset_period2_start_dt = offset_period1_end_dt
    offset_period2_end_dt = (datetime(day.timeframe[0].year, day.timeframe[0].month, day.timeframe[0].day, 19, 0, 0, tzinfo=day.timezone)+timedelta(days=1))

    offset_period1_start = offset_period1_start_dt.timestamp()
    offset_period1_end = offset_period1_end_dt.timestamp()

    offset_period2_start = offset_period2_start_dt.timestamp()
    offset_period2_end = offset_period2_end_dt.timestamp()

    n = len(day.sleep_states)

    # Need to adjust periods depending on epoch period
    epoch_period = day.timestamps[1] - day.timestamps[0]
    epochs_per_minute = 60 / epoch_period

    sleep_state = SleepWakeState.UNKNOWN
   
    sleep_onsets = list()
    sleep_offsets = list()

    awake_count = 0
    sleep_count = 0
    for i in range(0, n):
        # This epoch is scored as Sleep
        if (day.sleep_states[i] == 1):
            awake_count = 0
            # We are not currently asleep
            if sleep_state is not SleepWakeState.ASLEEP:
                #Increment the "been asleep for N epochs" counter
                sleep_count += 1
                
        # This epoch is scored as Awake
        elif (day.sleep_states[i] == 0):
            sleep_count = 0
            if sleep_state is not SleepWakeState.AWAKE:
                #Increment the "been awake for N epochs" counter
                awake_count += 1

        if sleep_count >= (sleep_onset_minutes*epochs_per_minute):
            if (day.timestamps[i] >= onset_period_start and day.timestamps[i] < onset_period_end):
                sleep_onsets.append(i-sleep_count+1)
                sleep_state = SleepWakeState.ASLEEP
                dt = datetime.fromtimestamp(day.timestamps[i-sleep_count+1], tz=day.timezone)
                sleep_count = 0

        # Period 1 is from 19:00 until 05:00  
        elif awake_count >= (sleep_offset_minutes_1*epochs_per_minute):
            if (day.timestamps[i] >= offset_period1_start and day.timestamps[i] < offset_period1_end):
                sleep_offsets.append(i-awake_count+1)
                sleep_state = SleepWakeState.AWAKE
                dt = datetime.fromtimestamp(day.timestamps[i-awake_count+1], tz=day.timezone)
                awake_count = 0
                
        # Period 2 is from 05:00 until 19:00
        elif awake_count >= (sleep_offset_minutes_2*epochs_per_minute):
            if (day.timestamps[i] >= offset_period2_start and day.timestamps[i] <= offset_period2_end):
                sleep_offsets.append(i-awake_count+1)
                sleep_state = SleepWakeState.AWAKE
                dt = datetime.fromtimestamp(day.timestamps[i-awake_count+1], tz=day.timezone)
                awake_count = 0


    if len(sleep_offsets) < len(sleep_onsets):
        sleep_offsets.append(n)
                    
    return sleep_onsets, sleep_offsets
