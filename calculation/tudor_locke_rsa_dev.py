# sleepy - sleep analysis
# Copyright (C) 2019  MRC Epidemiology Unit, University of Cambridge
#   
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
#   
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#   
# You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
from enum import Enum
from abc import ABC, abstractmethod
import numpy as np
import math
import statistics
from datetime import datetime, time, date, timedelta
import progressbar
from matplotlib import pyplot as plt
from matplotlib import dates as md
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle
from ..sleepy_utilities import *
from ..sleepy_calculation import *
from copy import deepcopy
import io
from ..sleepy_results import SleepParameters, SleepDay, SleepResult

def calculate_day(day, config = None):
    default_config = {
        'noon_to_noon' : True,
        'sleep_onset_minutes': 5,
        'sleep_onset_search_start': '19:00:00',
        'sleep_onset_search_end': '05:59:00',
        'sleep_offset_minutes_1': 10,
        'sleep_offset_search_start_1': '05:00:00',
        'sleep_offset_search_end_1': '11:58:00',
        'sleep_offset_minutes_2': 20,
        'sleep_offset_search_start_2': '21:40:00',
        'sleep_offset_search_end_2': '04:59:00',
        'min_sleep_minutes': 160,
        'min_wake_minutes': 20,
        'max_sleep_minutes': 1440,
        'min_nonzero_epochs': 0
    }

    config = set_config(config, default_config)

    sleep_parameters = SleepParameters()

    # If our time into bed is before our time out of bed then we are expecting our 24hr period to
    # contain our sleep period (i.e. noon to noon)
    # Otherwise we expect some sleep in the am from the previous night and some sleep in this pm
    noon_to_noon = False
    if day.time_in_bed < day.time_out_bed:
        noon_to_noon = True
    else:
        #TODO Figure out midnight to midnight
        raise RuntimeError('Currently TUDOR_LOCKE analysis is only configured for noon-to-noon analysis')

    min_wake_period_mins = config['min_wake_minutes']
    min_sleep_minutes = config['min_sleep_minutes']
    max_sleep_minutes = config['max_sleep_minutes']
    min_nonzero_epochs = config['min_nonzero_epochs']

    sleep_state = SleepWakeState.UNKNOWN

    n = len(day.sleep_states)

    # Need to adjust periods depending on epoch period
    epoch_period = day.timestamps[1] - day.timestamps[0]
    epochs_per_minute = 60 / epoch_period

    """
    Sleep Onsets = The starting point of the first 5 continuous minutes of sleep
    """
    sleep_onsets = list()
    if noon_to_noon:
        sleep_onset_minutes = 5
        sleep_onset_search_start = datetime.strptime(day.date.isoformat() + " 19:00:00",'%Y-%m-%d %H:%M:%S').timestamp()
        sleep_onset_search_end = datetime.strptime((day.date + timedelta(days=1)).isoformat() + " 05:59:00",'%Y-%m-%d %H:%M:%S').timestamp()
        _sleep_onsets = find_sleep_onsets_rsa(day, sleep_onset_search_start, sleep_onset_search_end, sleep_onset_minutes)
        sleep_onsets.extend(_sleep_onsets)
    else:
        #TODO Figure out midnight to midnight
        raise RuntimeError('Currently TUDOR_LOCKE analysis is only configured for noon-to-noon analysis')
        """
        sleep_onset_search_start_am = datetime.strptime((day.date - timedelta(days=1)).isoformat() + " 19:00:00",'%Y-%m-%d %H:%M:%S').timestamp()
        sleep_onset_search_end_am = datetime.strptime(day.date.isoformat() + " 05:59:00" ,'%Y-%m-%d %H:%M:%S').timestamp()

        sleep_onset_search_start_pm = datetime.strptime(day.date.isoformat() + " " + config['sleep_onset_search_start'],'%Y-%m-%d %H:%M:%S').timestamp()
        sleep_onset_search_end_pm = datetime.strptime((day.date + timedelta(days=1)).isoformat() + " " + config['sleep_onset_search_end'],'%Y-%m-%d %H:%M:%S').timestamp()
        """


    """
    Sleep Offsets:
        the first of 20 minutes awake (from 9:40pm until 4:59am)
        the first of 10 minutes awake (from 5:00am until 11:58am)
    """
    sleep_offsets = list()

    if noon_to_noon:
        sleep_offset_minutes = 20
        sleep_offset_search_start = datetime.strptime(day.date.isoformat() + " 21:40:00",'%Y-%m-%d %H:%M:%S').timestamp()
        sleep_offset_search_end = datetime.strptime((day.date + timedelta(days=1)).isoformat() + " 04:59:00",'%Y-%m-%d %H:%M:%S').timestamp()
        _sleep_offsets = find_sleep_offsets_rsa(day, sleep_offset_search_start, sleep_offset_search_end, sleep_offset_minutes)
        sleep_offsets.extend(_sleep_offsets)

        sleep_offset_minutes = 10
        sleep_offset_search_start = datetime.strptime((day.date + timedelta(days=1)).isoformat() + " 05:00:00",'%Y-%m-%d %H:%M:%S').timestamp()
        sleep_offset_search_end = datetime.strptime((day.date + timedelta(days=1)).isoformat() + " 11:58:00",'%Y-%m-%d %H:%M:%S').timestamp()
        _sleep_offsets = find_sleep_offsets_rsa(day, sleep_offset_search_start, sleep_offset_search_end, sleep_offset_minutes)
        sleep_offsets.extend(_sleep_offsets)

    else:
        #TODO Figure out midnight to midnight
        raise RuntimeError('Currently TUDOR_LOCKE analysis is only configured for noon-to-noon analysis')
    
    sleep_offsets.sort()

    """
    SPT calculations
    """
    sleep_episodes = []
    stop_sleep = 0
    for start_sleep in sleep_onsets:
        if start_sleep >= stop_sleep:
            # For each sleep onset find the end of the SPT
            inds = np.nonzero(np.array(sleep_offsets) > start_sleep)[0]
            if len(inds) > 0:
                stop_sleep = sleep_offsets[inds[0]]-1
                sleep_count = stop_sleep-start_sleep+1
                sleep_minutes = sleep_count/epochs_per_minute
                sleep_episodes.append([start_sleep, stop_sleep, sleep_minutes, datetime.fromtimestamp(day.timestamps[start_sleep]), datetime.fromtimestamp(day.timestamps[stop_sleep])])
                
    
    # Combine sleep periods with gaps of less than 20 minutes
    spt_separations = []
    for s in range(0, len(sleep_episodes)-1):
        spt_separations.append([sleep_episodes[s+1][0] - sleep_episodes[s][1], s, s+1])

    spt_sleep_episodes = []
    spt_separation_epochs = epochs_per_minute*20
    merging = False
    i = 0
    for spt_sep in spt_separations:
        i += 1
        if spt_sep[0] < spt_separation_epochs:
            if not merging:
                merging = True
                start_sleep = sleep_episodes[spt_sep[1]][0]
            if i == len(spt_separations):
                stop_sleep = sleep_episodes[spt_sep[2]][1]        
                sleep_count = stop_sleep-start_sleep+1
                sleep_minutes = sleep_count/epochs_per_minute
                spt_sleep_episodes.append([start_sleep, stop_sleep, sleep_minutes, datetime.fromtimestamp(day.timestamps[start_sleep]), datetime.fromtimestamp(day.timestamps[stop_sleep])])
                merging = False
        elif merging:    
            stop_sleep = sleep_episodes[spt_sep[2]][1]        
            sleep_count = stop_sleep-start_sleep+1
            sleep_minutes = sleep_count/epochs_per_minute
            spt_sleep_episodes.append([start_sleep, stop_sleep, sleep_minutes, datetime.fromtimestamp(day.timestamps[start_sleep]), datetime.fromtimestamp(day.timestamps[stop_sleep])])
            merging = False
        else:
            spt_sleep_episodes.append(sleep_episodes[spt_sep[1]])
            if i == len(spt_separations):
                spt_sleep_episodes.append(sleep_episodes[spt_sep[2]])
                     
    if len(sleep_episodes):
        sleep_onset = sleep_episodes[0][0]
        sleep_offset = sleep_episodes[-1][1]
        sleep_parameters.sleep_onset = datetime.fromtimestamp(day.timestamps[sleep_onset])
        sleep_parameters.sleep_offset = datetime.fromtimestamp(day.timestamps[sleep_offset])
        sleep_parameters.non_wear = find_non_wear_rsa(day, sleep_onset, sleep_offset+1)
    else:
        sleep_onset = None
        sleep_offset = None
        sleep_parameters.sleep_onset = None
        sleep_parameters.sleep_offset = None

    sleep_parameters.sleep_episodes = sleep_episodes


            
    day.sleep_parameters = sleep_parameters

    if len(sleep_episodes):
        sleep_onset_dt = md.date2num(sleep_parameters.sleep_onset)
        sleep_offset_dt = md.date2num(sleep_parameters.sleep_offset)

    dates = [md.date2num(datetime.fromtimestamp(ts)) for ts in day.timestamps]
    plt.plot(dates, day.activity_data, 'b-')
    mval = max(day.activity_data)
    plt.plot(dates, day.sleep_states*(mval/4), 'g-')
    if len(sleep_episodes):
        plt.plot([sleep_onset_dt], [0], 'r*')
        plt.plot([sleep_offset_dt], [0], 'c*')
    plt.xticks(rotation=25)
    locator = md.AutoDateLocator(minticks=6)
    formatter = md.DateFormatter('%H:%M')
    ax = plt.gca()
    ax.xaxis.set_major_locator(locator)
    ax.xaxis.set_major_formatter(formatter)
    plt.show()

    return day

    """
    Sleep Episodes
    Wake Episodes
    """
    sleep_episodes = []
    rsa_sleep_episodes = []
    wake_episodes = []
    rsa_wake_episodes = []
    if len(sleep_onsets) == 0:
        if len(sleep_offsets) > 0:
            first_onset = 0
            last_offset = np.max(sleep_offsets)
            sleep_epochs = last_offset-first_onset+1
            sleep_minutes = sleep_epochs/epochs_per_minute
            sleep_episodes.append([first_onset, last_offset, sleep_minutes, datetime.fromtimestamp(day.timestamps[first_onset]), datetime.fromtimestamp(day.timestamps[last_offset])])
                    
    elif len(sleep_offsets) == 0:
        first_onset = np.min(sleep_onsets)
        last_offset = n-1
        sleep_epochs = last_offset-first_onset+1
        sleep_minutes = sleep_epochs/epochs_per_minute
        sleep_episodes.append([first_onset, last_offset, sleep_minutes, datetime.fromtimestamp(day.timestamps[first_onset]), datetime.fromtimestamp(day.timestamps[last_offset])])
    else:
        first_onset = np.min(sleep_onsets)
        first_offset = np.min(sleep_offsets)
        last_onset = np.max(sleep_onsets)
        last_offset = np.max(sleep_offsets)
        if (first_offset < first_onset):
            sleep_onsets.append(0)
        if (last_onset > last_offset):
            sleep_offsets.append(n-1)

        sleep_onsets = np.sort(sleep_onsets)
        sleep_offsets = np.sort(sleep_offsets)

        # for each sleep onset find the next sleep offset
        i = 0
        for sleep_onset in sleep_onsets:
            if sleep_onset >= i:
                sleep_offset_inds = np.where(sleep_offsets > sleep_onset)[0].flatten()
                if len(sleep_offset_inds):
                    sleep_offset = sleep_offsets[sleep_offset_inds[0]]
                    sleep_epochs = sleep_offset-sleep_onset+1
                    sleep_minutes = sleep_epochs/epochs_per_minute
                    sleep_episodes.append([sleep_onset, sleep_offset, sleep_minutes, datetime.fromtimestamp(day.timestamps[sleep_onset]), datetime.fromtimestamp(day.timestamps[sleep_offset])])

                    i = sleep_offset+1

        # for each wake onset find the next sleep onset
        i = 0
        for wake_onset in sleep_offsets:
            if wake_onset >= i:
                wake_offset_inds = np.where(sleep_onsets > wake_onset)[0].flatten()
                if len(wake_offset_inds):
                    wake_offset = sleep_onsets[wake_offset_inds[0]]
                    wake_epochs = wake_offset-wake_onset+1
                    wake_minutes = wake_epochs/epochs_per_minute
                    wake_episodes.append([wake_onset, wake_offset, wake_minutes, datetime.fromtimestamp(day.timestamps[wake_onset]), datetime.fromtimestamp(day.timestamps[wake_offset])])

                    i = wake_offset+1    

        """
        Filter out non-compliant sleep episodes to determine full TUDOR_LOCKE sleep eposides
        """
        sleep_onset = sleep_episodes[0][0]
        for i in range(0,len(sleep_episodes)):
            if i == len(sleep_episodes)-1:
                sleep_offset = sleep_episodes[i][1]
                sleep_epochs = sleep_offset-sleep_onset+1
                sleep_minutes = sleep_epochs/epochs_per_minute
    
    """
    Sleep Onset        
        If we are analysing a day as between midnight to midnight then we expect the sleep onset to be beginning or end of the data
        If we are analysing a day as between noon to noon then we expect the sleep onset to be around the middle of the file
    """
    if noon_to_noon:
        rsa_sleep_episodes.append([sleep_onset, sleep_offset, sleep_minutes, datetime.fromtimestamp(day.timestamps[sleep_onset]), datetime.fromtimestamp(day.timestamps[sleep_offset])])
    else:
        wake_period = sleep_episodes[i+1][0] - sleep_episodes[i][1]
        if wake_period > 20:
            sleep_offset = sleep_episodes[i][1]
            sleep_epochs = sleep_offset-sleep_onset+1
            sleep_minutes = sleep_epochs/epochs_per_minute
            rsa_sleep_episodes.append([sleep_onset, sleep_offset, sleep_minutes, datetime.fromtimestamp(day.timestamps[sleep_onset]), datetime.fromtimestamp(day.timestamps[sleep_offset])])
            sleep_onset = sleep_episodes[i+1][0]                    

    """
    Filter out non-compliant wake episodes to determine full TUDOR_LOCKE wake eposides
    """
    for wake_episode in wake_episodes:
        if day.timestamps[wake_episode[0]] < sleep_offset_search_end_2_am and day.timestamps[wake_episode[1]] < sleep_offset_search_end_2_am:
            rsa_wake_episodes.append(wake_episode)
        elif day.timestamps[wake_episode[0]] > sleep_offset_search_start_2_pm and day.timestamps[wake_episode[1]] > sleep_offset_search_start_2_pm:
            rsa_wake_episodes.append(wake_episode)

    sleep_parameters.sleep_episodes = rsa_sleep_episodes
    sleep_parameters.wake_episodes = rsa_wake_episodes

    


    """
    sleep_onset_indices = []
    sleep_offset_indices = []

    consecutive_sleep_counter = 0
    consecutive_wake_counter = 0

    for s in range(n):
        if (sleep_states[s] == 1):
            consecutive_sleep_counter += 1
            consecutive_wake_counter = 0
        else:
            consecutive_wake_counter += 1
            consecutive_sleep_counter = 0

        if (consecutive_sleep_counter >= sleep_onset_post_sleep_epochs) and (sleep_state is not SleepWakeState.ASLEEP):
            ind = (s-consecutive_sleep_counter)+1in_bed_pm
            if in_between(datetime.fromtimestamp(day.timestamps[ind]).time(), sleep_onset_default, sleep_offset_default):
                sleep_onset_indices.append(ind)
                sleep_state = SleepWakeState.ASLEEP

        elif in_between(datetime.fromtimestamp(day.timestamps[s]).time(), sleep_offset_post_wake_start_1, sleep_offset_post_wake_end_1):
            if (consecutive_wake_counter >= sleep_offset_post_sleep_epochs_1) and (sleep_state is not SleepWakeState.AWAKE):
                ind = (s-consecutive_wake_counter)+1
                sleep_offset_indices.append(ind)
                sleep_state = SleepWakeState.AWAKE
        
        elif in_between(datetime.fromtimestamp(day.timestamps[s]).time(), sleep_offset_post_wake_start_2, sleep_offset_post_wake_end_2):
            if (consecutive_wake_counter >= sleep_offset_post_sleep_epochs_2) and (sleep_state is not SleepWakeState.AWAKE):
                ind = (s-consecutive_wake_counter)+1
                sleep_offset_indices.append(ind)
                sleep_state = SleepWakeState.AWAKE


    sleep_periods = np.zeros((0, 2))
    wake_periods = np.zeros((0, 2))

    if (len(sleep_onset_indices) == 0) or (len(sleep_offset_indices) == 0):
        return sleep_parameters

    if sleep_onset_indices[0] > sleep_offset_indices[0]:
        # First patch would not have a start
        sleep_onset_indices = np.append(0, sleep_onset_indices)

    if sleep_onset_indices[-1] > sleep_offset_indices[-1]:
        # Last patch would not have end
        sleep_offset_indices = np.append(sleep_offset_indices, len(sleep_timestamps)-1)

    if len(sleep_onset_indices) != len(sleep_offset_indices):
        raise RuntimeError('The onset and offset list count should match')

    # Find wakeful period
    wake_periods = []
    for i in range(0, len(sleep_onset_indices)-1):
        offset = sleep_offset_indices[i]
        next_onset = sleep_onset_indices[i+1]
        wake_period = (sleep_timestamps[next_onset]-sleep_timestamps[offset]) / 60
        wake_periods.append(wake_period)

    # Add a large period to force a break
    wake_periods.append(1000)

    i = 0
    combining = False
    for w in wake_periods:
        if w < min_wake_epochs:
            # Are we combining this with another period?  
            # If we are then we already have an onset
            if not combining:
                combining = True
                onset = sleep_onset_indices[i]
        else:
            offset = sleep_offset_indices[i]

            # Are we combining this with another period?  
            # If we are then we already have an onset
            if not combining:
                onset = sleep_onset_indices[i]

            # Is the sleep period long enough? Or does it include the first or last sleep epoch
            if ((offset-onset) > min_sleep_period_mins) \
                or (offset == len(sleep_timestamps)-1) \
                or (onset == 0):

                sleep_period = np.array([[sleep_timestamps[onset], sleep_timestamps[offset]]])
                sleep_periods = np.append(sleep_periods, sleep_period, axis=0)

            combining = False
        i += 1
    
    if len(sleep_periods) > 1:
        # The first sleep onset that isn't the very first timestamp is our main sleep onset event
        sleep_parameters.night_sleep_onset = sleep_periods[1][0]

        # The last sleep onset that isn't the very last timestamp is our main sleep onset event
        sleep_parameters.night_sleep_offset = sleep_periods[-2][1]

    sleep_parameters.wake_periods = wake_periods
    sleep_parameters.sleep_periods = sleep_periods
    """
    day.sleep_parameters = sleep_parameters
    day.valid = True

    return day


def find_sleep_onsets_rsa(day, sleep_onset_search_start, sleep_onset_search_end, sleep_onset_minutes):
    
    n = len(day.sleep_states)

    # Need to adjust periods depending on epoch period
    epoch_period = day.timestamps[1] - day.timestamps[0]
    epochs_per_minute = 60 / epoch_period

    sleep_onsets = list()
    # Look for sleep onsets in the am (7:00pm yesterday until 5:59am today)
    start_index = np.where(day.timestamps >= sleep_onset_search_start)[0].flatten()
    if len(start_index) == 0:
        start_index = 0
    else:
        start_index = start_index[0]

    end_index = np.where(day.timestamps >= sleep_onset_search_end)[0].flatten()
    if len(end_index) == 0:
        end_index = n-1
    else:
        end_index = end_index[0]
        
    asleep_epochs = 0
    sleep_onset = -1
    for i in range(start_index, end_index+1):
        if day.sleep_states[i] == 1:
            if asleep_epochs == 0:
                sleep_onset = i
            asleep_epochs += 1
        else:
            asleep_epochs = 0
        
        if asleep_epochs >= sleep_onset_minutes*epochs_per_minute and sleep_onset >= 0:
            sleep_onsets.append(sleep_onset)
            sleep_onset = -1

    return sleep_onsets

def find_sleep_offsets_rsa(day, sleep_offset_search_start, sleep_offset_search_end, sleep_offset_minutes):
    
    n = len(day.sleep_states)

    # Need to adjust periods depending on epoch period
    epoch_period = day.timestamps[1] - day.timestamps[0]
    epochs_per_minute = 60 / epoch_period

    sleep_offsets = list()
    start_index = np.where(day.timestamps >= sleep_offset_search_start)[0].flatten()
    if len(start_index) == 0:
        start_index = 0
    else:
        start_index = start_index[0]

    end_index = np.where(day.timestamps >= sleep_offset_search_end)[0].flatten()
    if len(end_index) == 0:
        end_index = n-1
    else:
        end_index = end_index[0]

    awake_epochs = 0
    sleep_offset = -1
    for i in range(start_index, end_index+1):
        if day.sleep_states[i] == 0:
            if awake_epochs == 0:
                sleep_offset = i
            awake_epochs += 1
        else:
            awake_epochs = 0
        
        if awake_epochs >= sleep_offset_minutes*epochs_per_minute and sleep_offset >= 0:
            sleep_offsets.append(sleep_offset)
            sleep_offset = -1

    return sleep_offsets

def find_non_wear_rsa(day, non_wear_search_start, non_wear_search_end):
    
    # Need to adjust periods depending on epoch period
    epoch_period = day.timestamps[1] - day.timestamps[0]
    epochs_per_minute = 60 / epoch_period

    non_wear = list()
    non_wear_counter = 0
    movement_counter = 0
    start_non_wear = -1
    for i in range(non_wear_search_start, non_wear_search_end):
        if day.activity_data[i] == 0:
            non_wear_counter += 1
            if start_non_wear < 0:
                start_non_wear = i
        else:
            movement_counter += 1
            non_wear_counter += 1
            if movement_counter > 2:
                if non_wear_counter >= 90:
                    stop_non_wear = i-1
                    non_wear_count = stop_non_wear-start_non_wear+1
                    non_wear_minutes = non_wear_count/epochs_per_minute
                    non_wear.append([start_non_wear, stop_non_wear, non_wear_minutes, datetime.fromtimestamp(day.timestamps[start_non_wear]), datetime.fromtimestamp(day.timestamps[stop_non_wear])])

                start_non_wear = -1
                non_wear_counter = 0
                movement_counter = 0
        
    if non_wear_counter >= 90:
        stop_non_wear = i
        non_wear_count = stop_non_wear-start_non_wear+1
        non_wear_minutes = non_wear_count/epochs_per_minute
        non_wear.append([start_non_wear, stop_non_wear, non_wear_minutes, datetime.fromtimestamp(day.timestamps[start_non_wear]), datetime.fromtimestamp(day.timestamps[stop_non_wear])])

    return non_wear