# sleepy - sleep analysis
# Copyright (C) 2019  MRC Epidemiology Unit, University of Cambridge
#   
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
#   
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#   
# You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
from enum import Enum
from abc import ABC, abstractmethod
import numpy as np
import math
import statistics
from datetime import datetime, time, date, timedelta
import progressbar
from matplotlib import pyplot as plt
from matplotlib import dates as md
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle
from ..sleepy_calculation import *
from ..sleepy_utilities import *
from copy import deepcopy
import io
from ..sleepy_results import SleepParameters, SleepDay, SleepResult

def calculate_day(day: SleepDay, config = None):
    # Defaults
    default_config = {
        'sleep_onset_minutes' : 20,
        'long_sleep_episode_mins': 5,
        'long_wake_episode_mins': 5
    }
    config = set_config(config, default_config)
        
    sleep_parameters = SleepParameters()

    # If our time into bed is before our time out of bed then we are expecting our 24hr period to
    # contain our sleep period (i.e. noon to noon)
    # Otherwise we expect some sleep in the am from the previous night and some sleep in this pm
    noon_to_noon = False
    if day.time_in_bed < day.time_out_bed:
        noon_to_noon = True
    
    long_sleep_episode_mins = config['long_sleep_episode_mins']
    sleep_onset_minutes = config['sleep_onset_minutes']
    long_wake_episode_mins = config['long_wake_episode_mins']

    n = len(day.sleep_states)

    # Need to adjust periods depending on epoch period
    epoch_period = day.timestamps[1] - day.timestamps[0]
    epochs_per_minute = 60 / epoch_period
    
    # find all sleep periods between time_in_bed and time_out_bed
    in_bed_pm = day.timestamps >= day.time_in_bed.timestamp()
    in_bed_pm_inds = np.nonzero(in_bed_pm)[0]
    in_bed_am = day.timestamps <= day.time_out_bed.timestamp()
    in_bed_am_inds = np.nonzero(in_bed_am)[0]
    in_bed_overnight = np.array(in_bed_am) & np.array(in_bed_pm)
    in_bed_overnight_inds = np.nonzero(in_bed_overnight)[0]
    
    if noon_to_noon:        
        in_bed_overnight_ind = np.min(np.nonzero(in_bed_overnight)[0])
        out_bed_overnight_ind = np.max(np.nonzero(in_bed_overnight)[0])
        time_in_bed_epochs = out_bed_overnight_ind - in_bed_overnight_ind +1
    else:
        in_bed_pm_ind = np.min(np.nonzero(in_bed_pm)[0])
        out_bed_pm_ind = np.max(np.nonzero(in_bed_pm)[0])
        time_in_bed_epochs_pm = out_bed_pm_ind - in_bed_pm_ind +1

        in_bed_am_ind = np.min(np.nonzero(in_bed_am)[0])
        out_bed_am_ind = np.max(np.nonzero(in_bed_am)[0])
        time_in_bed_epochs_am = out_bed_am_ind - in_bed_am_ind +1

        time_in_bed_epochs = time_in_bed_epochs_pm + time_in_bed_epochs_am

    sleep_parameters.timeInBed = time_in_bed_epochs/epochs_per_minute

    """
    Sleep Onset = The starting point of the first 20 continuous minutes of sleep, after lying in bed, with only 1 minute of wake in the 20 minute block       
        If we are analysing a day as between midnight to midnight then we expect the sleep onset to be at the end of the data maybe beginning
        If we are analysing a day as between noon to noon then we expect the sleep onset to be around the middle of the file
    """
    sleepOnset = -1
    sleepOnsetPm = -1
    sleepOnsetAm = -1
    if noon_to_noon:
        asleep_epochs = 0
        awake_epochs = 0        
        for i in range(in_bed_overnight_ind, out_bed_overnight_ind+1):
            if day.sleep_states[i] == 1:
                if asleep_epochs == 0:
                    sleepOnset = i
                asleep_epochs += 1
                awake_epochs = 0
            else:
                awake_epochs += 1
                if awake_epochs > 1*epochs_per_minute:
                    asleep_epochs = 0
                else:
                    asleep_epochs += 1
            
            if asleep_epochs >= sleep_onset_minutes*epochs_per_minute:
                break
    else:
        asleep_epochs = 0
        awake_epochs = 0
        for i in range(in_bed_pm_ind, out_bed_pm_ind+1):
            if day.sleep_states[i] == 1:
                if asleep_epochs == 0:
                    sleepOnsetPm = i
                asleep_epochs += 1
                awake_epochs = 0
            else:
                awake_epochs += 1
                if awake_epochs > 1*epochs_per_minute:
                    asleep_epochs = 0
                else:
                    asleep_epochs += 1
            
            if asleep_epochs >= sleep_onset_minutes*epochs_per_minute:
                break
        
        if (sleepOnset < 0):
            asleep_epochs = 0
            awake_epochs = 0
            for i in range(in_bed_am_ind, out_bed_am_ind+1):
                if day.sleep_states[i] == 1:
                    if asleep_epochs == 0:
                        sleepOnsetAm = i
                    asleep_epochs += 1
                    awake_epochs = 0
                else:
                    awake_epochs += 1
                    if awake_epochs > 1*epochs_per_minute:
                        asleep_epochs = 0
                    else:
                        asleep_epochs += 1
                
                if asleep_epochs >= sleep_onset_minutes*epochs_per_minute:
                    break

    if noon_to_noon:
        if sleepOnset >= 0:
            sleep_parameters.sleepOnset = datetime.fromtimestamp(day.timestamps[sleepOnset])
    else:
        if sleepOnsetPm >= 0:
            sleep_parameters.sleepOnset = datetime.fromtimestamp(day.timestamps[sleepOnsetPm])
        elif sleepOnsetPm >= 0:
            sleep_parameters.sleepOnset = datetime.fromtimestamp(day.timestamps[sleepOnsetAm])
             
    """
    Sleep Offset = The last time the subject was scored asleep before getting out of bed    
    """
    sleepOffset = -1
    sleepOffsetPm = -1
    sleepOffsetAm = -1
    if noon_to_noon:
        for i in range(out_bed_overnight_ind, in_bed_overnight_ind, -1):
            if day.sleep_states[i] == 1:
                sleepOffset = i
                break
    else:
        for i in range(out_bed_pm_ind, in_bed_pm_ind, -1):
            if day.sleep_states[i] == 1:
                sleepOffsetPm = i
                break

        for i in range(out_bed_am_ind, in_bed_am_ind, -1):
            if day.sleep_states[i] == 1:
                sleepOffsetAm = i
                break

    if noon_to_noon:
        if sleepOffset >= 0:
            sleep_parameters.sleepOffset = datetime.fromtimestamp(day.timestamps[sleepOffset])
    else:
        if sleepOffsetAm >= 0:
            sleep_parameters.sleepOffset = datetime.fromtimestamp(day.timestamps[sleepOffsetAm])        
        elif sleepOffsetPm >= 0:
            sleep_parameters.sleepOffset = datetime.fromtimestamp(day.timestamps[sleepOffsetPm])

    sleep_parameters.sleepOnsetAmInd = sleepOnsetAm
    sleep_parameters.sleepOffsetAmInd = sleepOffsetAm
    sleep_parameters.sleepOnsetPmInd = sleepOnsetPm
    sleep_parameters.sleepOffsetPmInd = sleepOffsetPm

    """
    O-O interval = 
    """
    if (noon_to_noon):
        sleep_parameters.sleepPeriodTime = (sleepOffset-sleepOnset+1) / epochs_per_minute
    else:
        if sleepOffsetAm >= 0:
            if sleepOnsetAm >= 0:
                sleep_epochs_am = (sleepOffsetAm - sleepOnsetAm)+1
            else:
                sleep_epochs_am = sleepOffsetAm + 1
        if sleepOnsetPm >= 0:
            if sleepOffsetPm >= 0:
                sleep_epochs_pm = (sleepOffsetPm-sleepOnsetPm)+1
            else:
                sleep_epochs_pm = n-sleepOnsetPm

        sleep_parameters.sleepPeriodTime = (sleep_epochs_am+sleep_epochs_pm) / epochs_per_minute

    """
    True sleep minutes (TSMIN) = Sum of all sleep minutes between sleep_onset and sleep_offset
    """
    if (noon_to_noon):
        sleep_parameters.totalSleepTime = np.sum(day.sleep_states[sleepOnset:sleepOffset+1])/epochs_per_minute
    else:
        if sleepOffsetAm >= 0:
            if sleepOnsetAm >= 0:
                tsmin_am = np.sum(day.sleep_states[sleepOnsetAm:sleepOffsetAm+1])/epochs_per_minute
            else:
                tsmin_am = np.sum(day.sleep_states[0:sleepOffsetAm+1])/epochs_per_minute

        if sleepOffsetPm >= 0:
            if sleepOnsetPm > 0:
                tsmin_pm = np.sum(day.sleep_states[sleepOnsetPm:sleepOffsetPm+1])/epochs_per_minute
            else:
                tsmin_pm = np.sum(day.sleep_states[sleepOnsetPm:n])/epochs_per_minute
        
        sleep_parameters.totalSleepTime = tsmin_am + tsmin_pm 
    
    """
    Sleep efficiency (SE) = totalSleepTime divided by the number of minutes between sleep_onset and sleep_offset then multiplied by 100
    """
    sleep_parameters.sleepEfficiencySPT = (sleep_parameters.totalSleepTime / sleep_parameters.sleepPeriodTime)*100
    

    """
    Sleep efficiency (SE) = totalSleepTime divided by the number of minutes between timeInBed and timeOutBed then multiplied by 100
    """
    sleep_parameters.sleepEfficiencyTIB = (sleep_parameters.totalSleepTime / sleep_parameters.timeInBed)*100
    
    """
    Sleep Onset Latency
    """    
    if noon_to_noon:
        sleep_parameters.sleepOnsetLatency = (sleepOnset - in_bed_overnight_ind)/epochs_per_minute
    else:
        if sleepOnsetPm >= 0:
            sleep_parameters.sleepOnsetLatency = (sleepOnsetPm - in_bed_pm_ind)/epochs_per_minute
        elif sleepOnsetAm >= 0:
            sleep_parameters.sleepOnsetLatency = ((n - in_bed_pm_ind) + sleepOffsetAm)/epochs_per_minute

    """
    Sleep Episodes
    """
    sleep_episodes=[]
    if noon_to_noon:
        sleep_count = 0
        for i in range(in_bed_overnight_ind, out_bed_overnight_ind+1):
            if day.sleep_states[i] == 1:                
                if sleep_count == 0:
                    start_sleep = i
                sleep_count += 1
            else:
                if sleep_count > 0:
                    sleep_minutes = sleep_count/epochs_per_minute
                    sleep_episodes.append([start_sleep, i-1, sleep_minutes, datetime.fromtimestamp(day.timestamps[start_sleep]), datetime.fromtimestamp(day.timestamps[i-1])])
                sleep_count = 0
                
            if i == out_bed_overnight_ind:
                if sleep_count > 0:
                    sleep_minutes = sleep_count/epochs_per_minute
                    sleep_episodes.append([start_sleep, i-1, sleep_minutes, datetime.fromtimestamp(day.timestamps[start_sleep]), datetime.fromtimestamp(day.timestamps[i-1])])
    else:
        sleep_count = 0
        for i in range(in_bed_pm_ind, out_bed_pm_ind+1):
            if day.sleep_states[i] == 1:                
                if sleep_count == 0:
                    start_sleep = i
                sleep_count += 1
            else:
                if sleep_count > 0:
                    sleep_minutes = sleep_count/epochs_per_minute
                    sleep_episodes.append([start_sleep, i-1, sleep_minutes, datetime.fromtimestamp(day.timestamps[start_sleep]), datetime.fromtimestamp(day.timestamps[i-1])])
                sleep_count = 0
                
            if i == out_bed_pm_ind:
                if sleep_count > 0:
                    sleep_minutes = sleep_count/epochs_per_minute
                    sleep_episodes.append([start_sleep, i-1, sleep_minutes, datetime.fromtimestamp(day.timestamps[start_sleep]), datetime.fromtimestamp(day.timestamps[i-1])])

        sleep_count = 0
        for i in range(in_bed_am_ind, out_bed_am_ind+1):
            if day.sleep_states[i] == 1:                
                if sleep_count == 0:
                    start_sleep = i
                sleep_count += 1
            else:
                if sleep_count > 0:
                    sleep_minutes = sleep_count/epochs_per_minute
                    sleep_episodes.append([start_sleep, i-1, sleep_minutes, datetime.fromtimestamp(day.timestamps[start_sleep]), datetime.fromtimestamp(day.timestamps[i-1])])
                sleep_count = 0
                
            if i == out_bed_am_ind:
                if sleep_count > 0:
                    sleep_minutes = sleep_count/epochs_per_minute
                    sleep_episodes.append([start_sleep, i-1, sleep_minutes, datetime.fromtimestamp(day.timestamps[start_sleep]), datetime.fromtimestamp(day.timestamps[i-1])])


    sleep_parameters.sleep_episodes = sleep_episodes

    # Wake minutes
    sleep_parameters.wakeAfterSleepOnset = sleep_parameters.sleepPeriodTime-sleep_parameters.totalSleepTime

    """
    Wake episodes
    """
    wake_episodes=[]
    if noon_to_noon:
        wake_count = 0
        for i in range(in_bed_overnight_ind, out_bed_overnight_ind+1):
            if day.sleep_states[i] == 0:                
                if wake_count == 0:
                    start_wake = i
                wake_count += 1
            else:
                if wake_count > 0:
                    wake_minutes = wake_count/epochs_per_minute
                    wake_episodes.append([start_wake, i-1, wake_minutes, datetime.fromtimestamp(day.timestamps[start_wake]), datetime.fromtimestamp(day.timestamps[i-1])])
                wake_count = 0

            if i == out_bed_overnight_ind:
                if wake_count > 0:
                    wake_minutes = wake_count/epochs_per_minute
                    wake_episodes.append([start_wake, i-1, wake_minutes, datetime.fromtimestamp(day.timestamps[start_wake]), datetime.fromtimestamp(day.timestamps[i-1])])
    else:
        wake_count = 0
        for i in range(in_bed_am_ind, out_bed_am_ind+1):
            if day.sleep_states[i] == 0:                
                if wake_count == 0:
                    start_wake = i
                wake_count += 1
            else:
                if wake_count > 0:
                    wake_minutes = wake_count/epochs_per_minute
                    wake_episodes.append([start_wake, i-1, wake_minutes, datetime.fromtimestamp(day.timestamps[start_wake]), datetime.fromtimestamp(day.timestamps[i-1])])
                wake_count = 0

            if i == out_bed_am_ind:
                if wake_count > 0:
                    wake_minutes = wake_count/epochs_per_minute
                    wake_episodes.append([start_wake, i-1, wake_minutes, datetime.fromtimestamp(day.timestamps[start_wake]), datetime.fromtimestamp(day.timestamps[i-1])])
         
        wake_count = 0
        for i in range(in_bed_pm_ind, out_bed_pm_ind+1):
            if day.sleep_states[i] == 0:                
                if wake_count == 0:
                    start_wake = i
                wake_count += 1
            else:
                if wake_count > 0:
                    wake_minutes = wake_count/epochs_per_minute
                    wake_episodes.append([start_wake, i-1, wake_minutes, datetime.fromtimestamp(day.timestamps[start_wake]), datetime.fromtimestamp(day.timestamps[i-1])])
                wake_count = 0

            if i == out_bed_pm_ind:
                if wake_count > 0:
                    wake_minutes = wake_count/epochs_per_minute
                    wake_episodes.append([start_wake, i-1, wake_minutes, datetime.fromtimestamp(day.timestamps[start_wake]), datetime.fromtimestamp(day.timestamps[i-1])])

    sleep_parameters.wake_episodes = wake_episodes
    sleep_parameters.wakenings = wake_episodes

    sleep_parameters.nightWakingFrequency = len(wake_episodes)

    nightWakingDuration = 0

    """
    Wake episode is:
        [start index, end index, duration in minutes, datetime of start, datetime of end]
    """
    for wake_episode in wake_episodes:
        nightWakingDuration += wake_episode[2]

    sleep_parameters.nightWakingDuration = nightWakingDuration
    
    day.sleep_parameters = sleep_parameters
    day.valid = True

    return day
