import numpy as np
import math
import statistics
from copy import copy
from enum import Enum
from .sleepy_utilities import *

class SleepyRescoringMethod(Enum):
    NONE = 0
    SIA = 1
    WEBSTER = 2
    COUNT_SCALED = 3

def rescore_count_scaled(sleep_states, epoch_period = 15):    
    rescored_sleep_states = deepcopy(sleep_states)
    n = len(sleep_states)
    n_samples_per_minute = int(np.ceil(60 / epoch_period))

    for i in range(0, n, n_samples_per_minute):
        if np.min(sleep_states[i:i+n_samples_per_minute]) == 0:
            rescored_sleep_states[i:i+n_samples_per_minute] = 0

    sleep_states = rescored_sleep_states

    return sleep_states

def rescore_inclinometer(sleep_states, inclinometer_data):
    rescored_sleep_states = deepcopy(sleep_states)
    if len(rescored_sleep_states) != len(inclinometer_data):
        raise RuntimeError('The sleep state data and the inclinometer data should have the same number of samples')

    # If the inclinometer is 'off' then recode the wake as sleep
    inclinometer_off = (inclinometer_data == 0)
    rescored_sleep_states[inclinometer_off] = 1

    sleep_states = rescored_sleep_states

    return sleep_states

def rescore_webster(sleep_states, epoch_period = 60):        

    rescored_sleep_states = deepcopy(sleep_states)
    """
    Based on the sleep epoch period apply the Webster et al. rescoring rules:
    
    A. After at least 4 minutes of wake - rescore the next 1 minute of sleep to wake
    B. After at least 10 minutes of wake  - rescore the next 3 minutes of sleep as wake
    C. After at least 15 minutes of wake - rescore the next 4 minutes of sleep as wake
    D. 6 minutes or less scored as sleep surrounded by at least 10 minutes (before and after) scored as wake -
        rescore as wake
    E. 10 minutes or less scored as sleep surrounded by at least 20 minutes (before and after) scored as wake -
        rescore as wake
    """
    rescored_sleep_states = np.array(rescored_sleep_states)
    n_samples_per_minute = 60 / epoch_period

    awake_4 = False
    awake_10 = False
    awake_15 = False
    before_10 = False
    before_20 = False
    before_10_index = -1
    before_20_index = -1
    awake_n = 0
    asleep_n = 0
    recoded_n = 0

    for i in range(0, len(sleep_states)):

        if sleep_states[i] == 0:
            awake_n += 1
            asleep_n = 0

            # This epoch is marked as awake
            if awake_n >= 20 * n_samples_per_minute:
                before_20 = True
                before_20_index = i
            if awake_n >= 15 * n_samples_per_minute:
                awake_15 = True
            if awake_n >= 10 * n_samples_per_minute:
                awake_10 = True
                before_10 = True
                before_10_index = i
            if awake_n >= 4 * n_samples_per_minute:
                awake_4 = True

        else:
            # This epoch is marked as asleep but we recode if any of the re-scoring conditions apply
            awake_n = 0
            asleep_n += 1
            recoded = False

            # Condition A
            if awake_4:
                if recoded_n < 1 * n_samples_per_minute:
                    if recoded == False:
                        rescored_sleep_states[i] = 0                    
                        recoded_n += 1
                        recoded = True
                else:
                    awake_4 = False

            # Condition B
            if awake_10:
                if recoded_n < 3 * n_samples_per_minute:
                    if recoded == False:
                        rescored_sleep_states[i] = 0                    
                        recoded_n += 1
                        recoded = True
                else:
                    awake_10 = False

            # Condition C
            if awake_15:
                if recoded_n < 4 * n_samples_per_minute:
                    if recoded == False:
                        rescored_sleep_states[i] = 0                    
                        recoded_n += 1
                        recoded = True
                else:
                    awake_15 = False

            # Condition D
            if before_10:
                # if we have had 6 minutes or less of sleep
                if asleep_n <= 6 * n_samples_per_minute:
                    end_index_10 = int(i + (n_samples_per_minute * 10) + 1)
                    if end_index_10 <= len(sleep_states):
                        # Check if any of the next 10 minutes are awake
                        awake_after = np.max(sleep_states[i + 1:end_index_10])
                        if awake_after == 0:
                            # recode all epochs back to the end of the previous block of 10 awake epochs
                            rescored_sleep_states[before_10_index:i+1] = 0
                            before_10 = False
                else:
                    # we have had more than 6 minutes of sleep so reset the before_10 flag
                    before_10 = False

            # Condition E
            if before_20:
                # if we have had 10 minutes or less of sleep
                if asleep_n <= 10 * n_samples_per_minute:
                    end_index_20 = int(i + (n_samples_per_minute * 20) + 1)
                    if end_index_20 <= len(sleep_states):
                        # Check if any of the next 20 minutes are awake
                        awake_after = np.max(sleep_states[i + 1:end_index_20])
                        if awake_after == 0:
                            # recode all epochs back to the end of the previous block of 20 awake epochs
                            rescored_sleep_states[before_20_index:i+1] = 0
                            before_10 = False
                else:
                    # we have had more than 10 minutes of sleep so reset the before_20 flag
                    before_20 = False
            
            if recoded == False:
                recoded_n = 0

        sleep_states = rescored_sleep_states

    return sleep_states