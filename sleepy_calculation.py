# sleepy - sleep analysis
# Copyright (C) 2019  MRC Epidemiology Unit, University of Cambridge
#   
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
#   
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#   
# You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
from enum import Enum
from abc import ABC, abstractmethod
import numpy as np
import math
import statistics
from datetime import datetime, time, date, timedelta
import progressbar
from matplotlib import pyplot as plt
from matplotlib import dates as md
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle
from .sleepy_utilities import *
from copy import deepcopy
import io
from .sleepy_results import SleepParameters, SleepDay, SleepResult

class SleepWakeState(Enum):
    ASLEEP = 0
    AWAKE = 1
    UNKNOWN = 9

class SleepyCalculationMethod(Enum):
    COUNT_SCALED = 1
    COLE_KRIPKE = 2
    TUDOR_LOCKE = 3
    TUDOR_LOCKE_RSA = 4
    LOG_ACC = 5
    SADEH = 6

