import numpy as np
import math
from copy import copy
import statistics
from enum import Enum
from .sleepy_utilities import *

class SleepyScoringMethod(Enum):
    COUNT_SCALED = 1
    COLE_KRIPKE = 2
    SADEH = 3

def score_count_scaled(activity_data, epoch_period):

    sleep_data = deepcopy(activity_data)

    #TODO - deal with special cases at beginning and end
    if epoch_period != 15:
        print(f'WARNING - Count-Scaled is expecting an epoch period of 15 seconds - currently {epoch_period}s')
        
    non_zero = sleep_data > 0

    inds = np.nonzero(non_zero)

    scalar = 1 / np.mean(sleep_data[inds])

    count_scaled = sleep_data * scalar

    n = len(count_scaled)

    #w = np.array([1.17, 1.09, 2.57, 4.30, 5.05, 4.01, 0.82])
    w = np.array([1.0, 2.0, 3.0, 4.0, 5.0, 3.0, 1.0])

    s = 2.7

    sleep_states = np.ones(n)

    for i in range(0, n):

        if i > 3:
            a1 = count_scaled[i - 4] * w[0]
        else:
            a1 = 0

        if i > 2:
            a2 = count_scaled[i - 3] * w[1]
        else:
            a2 = 0

        if i > 1:
            a3 = count_scaled[i - 2] * w[2]
        else:
            a3 = 0

        if i > 0:
            a4 = count_scaled[i - 1] * w[3]
        else:
            a4 = 0

        a5 = count_scaled[i] * w[4]

        if i < n - 1:
            a6 = count_scaled[i + 1] * w[5]
        else:
            a6 = 0
            
        if i < n - 2:
            a7 = count_scaled[i + 2] * w[6]
        else:
            a7 = 0

        a = (a1 + a2 + a3 + a4 + a5 + a6 + a7)

        if a >= s:
            sleep_states[i] = 0
    
    return sleep_states

def score_cole_kripke(activity_data, epoch_period):

    sleep_data = deepcopy(activity_data)

    n = len(sleep_data)

    if epoch_period == 60:
        #w = np.array([106, 54, 58, 76, 230, 74, 67])
        #s = 0.001
        w = np.array([1.06, .54, .58, .76, 2.30, .74, .67])
        s = 0.0033
    elif epoch_period == 30:
        w = np.array([50, 30, 14, 28, 121, 8, 50])
        s = 0.0001
    elif epoch_period == 10:
        w = np.array([550, 378, 413, 699, 1736, 287, 309])
        s = 0.00001
    else:
        print(f'WARNING - Cole-Kripke is expecting an epoch period of 60, 30 or 10 seconds - currently {epoch_period}s')
        if epoch_period > 30:
            print(f'    Continuing with 60 second parameters')
            w = np.array([106, 54, 58, 76, 230, 74, 67])
            s = 0.001
        elif epoch_period > 10:
            print(f'    Continuing with 30 second parameters')
            w = np.array([50, 30, 14, 28, 121, 8, 50])
            s = 0.0001
        else:
            print(f'    Continuing with 10 second parameters')
            w = np.array([550, 378, 413, 699, 1736, 287, 309])
            s = 0.00001

    sleep_states = np.zeros(n, np.int8)
    
    for i in range(0, n):
        # Special cases at the beginning
        if i > 3:
            an4 = sleep_data[i - 4] * w[0]
        else:
            an4 = 0

        if i > 2:
            an3 = sleep_data[i - 3] * w[1]
        else:
            an3 = 0

        if i > 1:
            an2 = sleep_data[i - 2] * w[2]
        else:
            an2 = 0

        if i > 0:
            an1 = sleep_data[i - 1] * w[3]
        else:
            an1 = 0

        a0 = sleep_data[i + 0] * w[4]

        # Special cases at the end    
        if i < n - 1:
            a1 = sleep_data[i + 1] * w[5]
        else:
            a1 = 0

        if i < n - 2:
            a2 = sleep_data[i + 2] * w[6]
        else:
            a2 = 0

        a = (an4 + an3 + an2 + an1 + a0 + a1 + a2) * s

        if a < 1:
            sleep_states[i] = 1
                

    return sleep_states

def score_sadeh(activity_data, epoch_period):

    sleep_data = deepcopy(activity_data)
    
    if epoch_period != 60:
        print(f'WARNING - Sadeh is expecting an epoch period of 60 seconds - currently {epoch_period}s')

    #
    #  PS:
    #     PS = Probability of sleep
    #     PS = 7.601 - 0.065*MW5 - 1.08*NAT - 0.056*SD6 - 0.703*LOGact
    #     if PS >= 0 then SLEEP else AWAKE
    #
    # MW5:
    #     Mean-W-5-min is the average number of activity counts during the scored epoch
    #     and the window of five epochs preceding and following it
    #
    # SD6:
    #     SD-last 6 min is the standard deviation of the activity counts during the scored
    #     epoch and the five epochs preceding it
    #
    # NAT:
    #     NAT is the number of epochs with activity level equal to or higher
    #     than 50 but lower than 100 activity counts in a window of 11 minutes
    #     that includes the scored epoch and the five epochs preceding and
    #     following it
    #
    # LOGact:
    #     LOG-Act is the natural logarithm of the number of activity counts during
    #     the scored epoch + 1.
    
    N = len(sleep_data)
    sleep_data[sleep_data > 300] = 300

    sleep_states = np.zeros(N)

    for i in range(0, N):
        if (i < 5):
            i_p5 = 0
        else:
            i_p5 = float(sleep_data[i - 5])

        if (i < 4):
            i_p4 = 0
        else:
            i_p4 = float(sleep_data[i - 4])

        if (i < 3):
            i_p3 = 0
        else:
            i_p3 = float(sleep_data[i - 3])

        if (i < 2):
            i_p2 = 0
        else:
            i_p2 = float(sleep_data[i - 2])

        if (i < 1):
            i_p1 = 0
        else:
            i_p1 = float(sleep_data[i - 1])

        i_0 = float(sleep_data[i])

        if (i > N - 2):
            i_n1 = 0
        else:
            i_n1 = float(sleep_data[i + 1])

        if (i > N - 3):
            i_n2 = 0
        else:
            i_n2 = float(sleep_data[i + 2])

        if (i > N - 4):
            i_n3 = 0
        else:
            i_n3 = float(sleep_data[i + 3])

        if (i > N - 5):
            i_n4 = 0
        else:
            i_n4 = float(sleep_data[i + 4])

        if (i > N - 6):
            i_n5 = 0
        else:
            i_n5 = float(sleep_data[i + 5])

        MW5 = (i_p5 + i_p4 + i_p3 + i_p2 + i_p1 + i_0 + i_n1 + i_n2 + i_n3 + i_n4 + i_n5) / 11.0
        SD6 = statistics.stdev([i_p5, i_p4, i_p3, i_p2, i_p1, i_0])
        NAT = 0
        for w in [i_p5, i_p4, i_p3, i_p2, i_p1, i_0, i_n1, i_n2, i_n3, i_n4, i_n5]:
            if (w >= 50) and (w < 100):
                NAT += 1

        LOGact = math.log(i_0 + 1)

        PS = 7.601 - 0.065 * MW5 - 1.08 * NAT - 0.056 * SD6 - 0.703 * LOGact

        if PS > -4:
            sleep_states[i] = 1
        else:
            sleep_states[i] = 0        

    return sleep_states
